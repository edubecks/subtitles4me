function download_subtitle(e) {
    e.preventDefault();
    $('#information_message').hide(200);
    var id = $(this).children('input:hidden').val();
//    console.log(id);
    $.ajax({
        dataType: 'json',
        url: '/download/id/' + id,
        type: 'GET',
        success: function (response) {
//            console.log(response);
            if (response['status']) {
                window.location.href = response['download_link'];
            }
            else {
                $('#information_message').html('We\'re currently in beta. Because of this, there is a ' +
                    'limit of 25 subtitle downloads per ' +
                    'day. We\'re planning to increase this value over time. If you think you need more ' +
                    'than this limit, feel free to ' +
                    '<a href="/contact/">contact us</a>').show(200);
            }
        }
    });

}
$(document).on('click', '.download_button', download_subtitle);

// corregir barra de descarga
$(function () {
    $(".score").each(function () {
        var $percentage = $(this).text() * 100;
        $(this).append($percentage);

    });
});

function hide_results(e) {
    $('#results').hide();
    $('#information_message').hide();
    $('body,html').animate({
        scrollTop: 0
    }, 400);
}
function hide_results_on_esc_key(e) {
    if (e.keyCode == 27) {
        hide_results(e);
    }
}
$(document).keyup(hide_results_on_esc_key);
$(document).on('click', '.close_results', hide_results);


var li_cache, over = false;

$("figure")
    .delegate("figcaption", "mouseenter", function (e) {
        var $li = $(this), speed;

        if (li_cache === this && over) {
            $.doTimeout("hoverOut");
            return;
        }

        if (over) {
            $.doTimeout("hoverOut", true);
            speed = 0;
        } else {
            $.doTimeout("hoverOut");
            speed = 500;
        }

        $.doTimeout("hoverIn", speed, function () {
            over = true;
            $li.find("div").fadeTo(200, 1.0);
        });
    })
    .delegate("figcaption", "mouseleave", function (e) {
        var $li = $(this);

        $.doTimeout("hoverIn");
        $.doTimeout("hoverOut", 500, function () {
            over = false;
            $li.find("div").stop(true).fadeOut();
        });
    });

/* mouse over in description*/
$(function () {
    $(".description").tipTip({maxWidth: "auto", edgeOffset: 10});
});

//
//
//// hide if clicking outside resuls
//
//
//
//function click_outside_results(e) {
//    var container = $("#results");
//    if (container.has(e.target).length === 0) {
//        container.hide();
//        $('#information_message').hide();
//        if ($('#results').css('display') != 'none') {
//            $('body,html').animate({
//                scrollTop: 0
//            }, 800);
//        }
//    }
//}
//$(document).mouseup(click_outside_results);


$(document).ready(function () {

    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

});
