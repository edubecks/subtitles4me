/**
 * User: edubecks
 * Date: 3/26/13
 * Time: 10:40 AM
 */


function handleFileClick(evt) {
    var files = evt.target.files; // FileList object

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0;i < files.length ; i++) {
        var f = files[i];
        output.push(f.name);
    }
    var list_files = output.join('\n');
//    console.log(list_files);
    $('#search_box').val(list_files);

}


function handleFileSelect(evt) {
//    console.log('handleFileSelect');
    evt.stopPropagation();
    evt.preventDefault();

    $('#drop_here').css('display','none');
    var files = evt.dataTransfer.files; // FileList object.

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
        output.push(f.name);
    }
    var list_files = output.join('\n');
//    console.log(list_files);
    $('#search_box').val(list_files);
//    document.getElementById('search_box').innerHTML = '<ul>' + output.join('') + '</ul>';
}

function handleDragOver(evt) {
//    console.log('handleDragOver');
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    $('#drop_here').css('display','block');
}



$(document).ready(function () {

    // bug
//    console.log('resizing files');
//    $('#files').css('width', '700px');
//    $('#files').css('height', '100%');
//    $('#files').css('margin-left', '300px');

//    console.log('drag n drop');
//    var dropZone = document.getElementById('drag_n_drop');
    var dropZone = document.getElementById('tv_body');
//    console.log(dropZone);
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
    document.getElementById('files').addEventListener('change', handleFileClick, false);
});

$(document).on('click','#drag_n_drop',function(){
    $('#files').click();
});

$(document).on('click','#select_file',function(){
    $('#files').click();
});


