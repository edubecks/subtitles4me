/**
 * User: edubecks
 * Date: 10/28/12
 *
 *
 * Time: 9:17 PM
 */

function load_results_in_the_same_page(data) {
    $('#results_content').html(data);
    $(".description").tipTip();
    $('#results').show();
    $('html, body').animate({
        scrollTop: '790px'
    }, 'slow');
}

function show_information_message(msg) {
    $('#information_message').html(msg);
    $("#information_message").show(100);
    $('html, body').animate({
        scrollTop: '510px'
    }, 'slow');
}

function search_button(e) {

    $('#results').hide();

    e.preventDefault();

    var search_terms = $.trim($('#search_box').val());
    var num_languages = $('input[name="language"]:checked').length;

    if (!search_terms) {
        // the user needs to fill some file
        show_information_message('No search terms.');
    }
    else if (num_languages <= 0) {
        // no languages were selected
        show_information_message('Please select one language.');
    }
    else if (num_languages > 3) {
        // number of languages exceeded
        show_information_message('No more than 3 languages are allowed.');
    } else {
        // everything is OK


        // getting languages array
        var languages = [];
        $("input[name=language]:checked").each(function () {
            languages.push($(this).val());
        });

        //set cookies
        $.cookie('languages', languages);

        var data = {
            'filename': search_terms,
            'languages': languages
        };

        // hide information message if it was shown before
        var url = '/search/';
        $("#information_message").hide(200);

        // set loading animation
        $('body').addClass("loading");

        $.ajax({
            url: url,
            data: data,
            traditional: true,
            success: function (data) {
                load_results_in_the_same_page(data);
                try {
                FB.XFBML.parse();
                } catch (ex) {
                }
                // remove animation
                $('body').removeClass("loading");
            },
            error: function () {
                // remove animation
                $('body').removeClass("loading");
                show_information_message('No results were found.');
            }
        });


    }
}
// search button
$(document).on('click', '#search_button', search_button);


function toggle_search_engine(evt) {
    $(this).closest("span").toggleClass("search_engine_selected");
    if (evt.target.type !== 'checkbox') {
        var $checkbox = $(this).children('input');
        $checkbox.prop('checked', !$checkbox.prop('checked'));
    }
}
//$(document).on('click', '.language', toggle_search_engine);


function load_default_parameters() {

    // load default languages
    if ($.cookie('languages')) {
        var languages = $.cookie('languages').split(',');
        for (var i = 0; i < languages.length; i++) {
            $('input[name="language"][value="' + languages[i] + '"]').prop('checked', true);
        }
    }
}
$(document).ready(load_default_parameters);

//$(document).on('hover', '#menu_beta', beta_hover);


function select_text_when_clicking() {

    $('#drop_here').css('display', 'none');

    var $this = $(this);
    $this.select();

    // Work around Chrome's little problem
    $this.mouseup(function () {
        // Prevent further mouseup intervention
        $this.unbind("mouseup");
        return false;
    });
}
$(document).on('focus', '#search_box', select_text_when_clicking);


