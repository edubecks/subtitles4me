/**
 * User: edubecks
 * Date: 10/27/12
 * Time: 12:04 PM
 */

function validate_single_filename(filename) {

    // replace path
    filename = filename.replace(/^.*[\\\/]/gi, "");
    // replace extension
    filename = filename.replace(/(.avi$|.mp4$|.mkv$)/, "");
    result = parse_movie(filename);
    var result;
    if (!result) {
        result = parse_tv_episode(filename);
        if (!result) {
            result = parse_default(filename);
        }
    }
    // trim punctuations from name and release
    result['title'] = $.trim(result['title'].replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,' ')).replace(/ /g,'.');
    result['release'] = $.trim(result['release'].replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,' ')).replace(/ /g,'.');

    return result;
}
function validate_multiple_filename(list_of_files) {
    var parsed_files = [];
    for (var i = 0; i < list_of_files.length; i++) {
        parsed_files[i] = validate_single_filename(list_of_files[i]);
    }
    return parsed_files;
}

function autocomplete_fields(result) {
    $("#title").val(result['title']);
    $("#season").val(result['season']);
    $("#episode").val(result['episode']);
    $("#release").val(result['release']);
    $("#year").val(result['year']);
}

function parse_tv_episode(filename) {
    var REGEX_EXPRESSIONS_SERIES = new Array(
        /(.*)[Ss]([0-9]+)[._-]*[Ee]([0-9]+)([^\\\\/]*)/,
        /*  foo.1x09*/
        /(.*)[\._ \-]([0-9]+)x([0-9]+)([^\\/]*)/,
        /*  foo.109*/
        /(.*)[\._ \-]([0-9]+)([0-9][0-9])[\._ \-]([^\\/]*)/,
        /(.*)[\._ \-]([0-9]+)([0-9][0-9])[\._ \-]([^\\/]*)/,
        /(.*)[\\\\/\\._ -]([0-9]+)([0-9][0-9])[\._ \-]([^\\/]*)/,
        /(.*)Season ([0-9]+) - Episode ([0-9]+)([^\\/])*/,
        /(.*)[\\\\/\\._ -][0]*([0-9]+)x[0]*([0-9]+)([^\\/])*/,
        /* foo_[s01]_[e01]*/
        /(.*)[[Ss]([0-9]+)\]_\[[Ee]([0-9]+)([^\\/]*)/,
        /* foo, s01e01, foo.s01.e01, foo.s01-e01*/
        /(.*)[\._ \-][Ss]([0-9]+)[\.\-\s]?[Ee]([0-9]+)([^\\/]*)/,
        /* foo - s01ep03, foo - s1ep03*/
        /(.*)s([0-9]+)ep([0-9]+)([^\\/]*)/,
        /(.*)[Ss]([0-9]+)[ ._-]*[Ee]([0-9]+)([^\\\\/]*)$/,
        /(.*)[\\\\/\\._ \\[\\(-]([0-9]+)x([0-9]+)([^\\\\/]*)$/
    );
    // series
    for (var i = 0; i < REGEX_EXPRESSIONS_SERIES.length; i++) {
        var result = REGEX_EXPRESSIONS_SERIES[i].exec(filename, 'i');
        if (result) {
            return {'title':result[1],
                'season':result[2],
                'episode':result[3],
                'release':result[4],
                'search_type':'tv_multiple',
                'year':''
            };
        }
    }



    return false;
}

function parse_movie(filename) {

    var REGEX_EXPRESSIONS_MOVIES = [
        /(.*)[\s_\-\(\.\[]+(\d{4})[\s_\-\)\.\]]+(.*)/,
        /(.*)[\s_\-\(\.]+(UNRATED[\s_\-\)\.]+.*)/,
        /(.*)[\s_\-\(\.]+(DVDRiP[\s_\-\)\.]+.*)/
    ];
    //movies
    for (var i = 0; i < REGEX_EXPRESSIONS_MOVIES.length; i++) {
        var result = REGEX_EXPRESSIONS_MOVIES[i].exec(filename, 'i');
        if (result) {
            var result_dict = { 'title':result[1],
                'search_type':'general',
                'year':'',
                'release':'',
                'season':'',
                'episode':''
            };
            if (result.length > 3) {
                result_dict['year'] = result[2];
                result_dict['release'] = result[3];
            }
            else {
                result_dict['release'] = result[2];
            }
            return result_dict;
        }
    }
    return false;
}

function parse_default(filename) {
    return {
        'title':filename,
        'search_type':'general',
        'year':'',
        'release':'',
        'season':'',
        'episode':''
    };
}

function dict_to_python_dict_string(dict){
    return (
        '{"title":"'+dict['title']+
        '","year":"'+dict['year']+
        '","release":"'+dict['release']+
        '","season":"'+dict['season']+
        '","episode":"'+dict['episode']
        +'"}'
        )
}

$('#search_box').keyup(validate_and_autocomplete).change(validate_and_autocomplete);
function validate_and_autocomplete(event) {
    console.log('here');
    var filename = $("#search_box").val();
    console.log('parsing ...' + filename);
    var search_type = $('input[name=download_selector]:checked').val();
    if (search_type != 'tv_multiple') {
        var result = validate_single_filename(filename);
        autocomplete_fields(result);
    }
}
