# coding: utf-8

__author__ = 'edubecks'

LANGUAGES = ['es', 'pb', 'en']
def ad_placer(results):
    """
    set the number of ads for each table of results
    max 2 ads for all tables
    """

    ad_places = {lang:0 for lang in LANGUAGES}
    ad_counter = 1
    for language, results_per_language in results.iteritems():
        if results_per_language['counter'] <= 1:
            if ad_counter > 0:
                ad_places[language] = 1
                ad_counter -= 1
    if ad_counter == 0:
        return ad_places
    else:
        for language, results_per_language in results.iteritems():
            ad_places[language] = ad_counter
            return ad_places
    return ad_places