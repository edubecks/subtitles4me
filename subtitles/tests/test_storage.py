# coding: utf-8
__author__ = 'edubecks'

from django.test import TestCase

class StorageTestCase(TestCase):

    def test_amazons3(self):

        from subtitles.model.storage.amazons3 import S3Storage
        storage  = S3Storage()
        storage.open_connection()
        url = storage.store('00000000001','http://www.subdivx.com/sub7/331779.rar')
        print url
        storage.close_connection()

        return


    def test_googlecloud(self):

        from subtitles.model.storage.googlecloud import GoogleCloudStorage
        storage  = GoogleCloudStorage()
        storage.open_connection()
        url = storage.store('00000000001','http://www.subdivx.com/sub7/331779.rar')
        print url
        storage.close_connection()
        return