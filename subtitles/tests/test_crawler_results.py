# coding: utf-8
from pprint import pprint
from subtitles.model.crawler.crawler import Crawler
from subtitles.model.parser import Parser
from subtitles.model.persistence import Persistence
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.sub_controller import SubController
from subtitles.model.subtitle_info import SubtitleInfo

__author__ = 'edubecks'

from django.test import TestCase


class CrawlerResultsTestCase(TestCase):
    def setUp(self):
        self.filenames = [
            {
                "filename": "Modern.Family.S04E04.HDTV.x264-LOL"
            },
            {
                "filename": "Spartacus S03E02 720p HDTV x264-IMMERSE"
            },
            {
                "filename": "Grimm S02E14 HDTV x264-LOL"
            },
        ]

        for filename in self.filenames:
            hash_id = Parser.parse_single_file(filename['filename'])['hash_id']
            Persistence.enqueue_result(hash_id)
        return

    def __check_results(self, sub_controller):

        spanish = SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1]

        ## filename0
        for filename in self.filenames:
            parsed_file = Parser.parse_single_file(filename=filename['filename'])

            ## results for filename0
            results = sub_controller.compute_results_single(parsed_file, max_results=20, analytics=False)

            ## spanish results
            self.assertIsNot(
                len(results[spanish]['results']),
                0,
                'Results not found!')
            print 'retrieving results for ', parsed_file['filename'], \
                'found', len(results[spanish]['results'])
            print results[spanish]['results']

        return


    def test_crawl_results(self):

        ## single thread
        num_threads=1
        Crawler.crawl_results(num_threads=num_threads, within_hours=24)
        Crawler.crawl_links(user=0, num_threads=num_threads, limit=100, cloud_storage=False)
        self.__check_results()




    def test_crawl_batch_subtitles(self):

        self.filenames = [
            {
                "filename": "Modern.Family.S04E04.HDTV.x264-LOL"
            },
            {
                "filename": "Game of Thrones S03E09 HDTV x264-LOL"
            },
            # {
            #     "filename": "Game of Thrones S03E08 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E07 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E06 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E05 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E04 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E03 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E02 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Game of Thrones S03E01 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Grimm S02E14 HDTV x264-LOL"
            # },
        ]

        for filename in self.filenames:
            hash_id = Parser.parse_single_file(filename['filename'])['hash_id']
            Persistence.enqueue_result(hash_id)

        ## sub_controller
        sites = [Subdivx.NAME]
        spanish = SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1]
        languages = [spanish]
        sub_controller = SubController(languages,
                                       sites,
                                       user=0,
                                       search=True,
                                       download=True,
                                       cloud_storage=None)

        ## multiple thread
        num_threads = 4
        Crawler.crawl_results(num_threads=num_threads,
                              within_hours=24,
                              sub_controller=sub_controller)
        Crawler.crawl_links(user=0,
                            num_threads=num_threads,
                            limit=500,
                            cloud_storage=False,
                            sub_controller=sub_controller
        )


        ## checking results
        spanish = SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1]

        ## filename0
        for filename in self.filenames:
            parsed_file = Parser.parse_single_file(filename=filename['filename'])

            ## results for filename0
            results = sub_controller.compute_results_single(parsed_file, max_results=20, analytics=False)

            ## spanish results
            self.assertIsNot(
                len(results[spanish]['results']),
                0,
                'Results not found!')
            print 'retrieving results for ', parsed_file['filename'], \
                'found', len(results[spanish]['results'])
            print results[spanish]['results']

        return