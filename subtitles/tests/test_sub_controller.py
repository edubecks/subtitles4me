# coding: utf-8
from pprint import pprint
from subtitles.model.crawler.crawler import Crawler
from subtitles.model.parser import Parser
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.sub_controller import SubController
from subtitles.model.subtitle_info import SubtitleInfo

__author__ = 'edubecks'

from django.test import TestCase

class SubControllerTestCase(TestCase):

    def setUp(self):
        return

    def test_crawl_results(self):

        sites = [Subdivx.NAME]
        spanish = SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1]
        languages = [spanish]

        controller = SubController(languages=languages,
                                   sites=sites,
                                   search=True,
                                   download=False
        )

        filename = "Modern.Family.S04E04.HDTV.x264-LOL"

        ## analyzing filename
        parsed_file = Parser.parse_single_file(filename=filename)

        ## crawling
        controller.crawler_results(parsed_file)

        ## getting subtitles
        Crawler.crawl_links(user=0, num_threads=1, limit=30, cloud_storage=False)

        results = controller.compute_results_single(parsed_file, max_results=20)

        ## spanish results
        self.assertIsNot(
            results[spanish]['results'],
            [],
            'Results not found!')
        pprint(results)

        return
    
    def test_enqueue(self):
        filenames = ['the vampire diaries s04e23 ',
         'm&amp aacute s all&amp aacute de los sue&amp ntilde os   1998',
         'the vampire diaries s04e07 ',
         'warum m&amp auml nner nicht zuh&amp ouml ren und frauen schlecht einpark   ',
         'barbapap&amp agrave s01e01 ',
         'sem perd&amp atilde o   2013',
         'o &amp uacute ltimo trem   2008',
         'homem de a&amp ccedil o   ',
         'reencarna&amp ccedil &amp atilde o nicole kidman dvdrip h264 1280 72 ',
         'jack o ca&amp ccedil ador de gigantes   ',
         'branca de neve e o ca&amp ccedil ador   ',
         'ref&amp eacute ns nicolas cage e nicole kidman dvdrip h264 1280 7 ',
         'percy jackson y el ladr&amp oacute n del rayo   2010',
         'ernest goes to camp   1987',
         'el padrino &amp eacute pico vhsrip   ',
         'rampage   2009',
         'pride &amp amp prejudice   2005',
         'pain &amp amp gain   2013',
         'jo&amp atilde o e maria ca&amp ccedil adores de bruxas legendado   ',
         'fantasia norsub &amp norwegian djevojka   ',
         'ca&amp ccedil a aos g&amp acirc ngsteres   ']



        for filename in filenames:
            SubController.enqueue_results(filename)

        return