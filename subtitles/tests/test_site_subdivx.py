# coding: utf-8
from pprint import pprint
from subtitles.model.parser import Parser
from subtitles.model.persistence import Persistence
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.subtitle_info import SubtitleInfo

__author__ = 'edubecks'

from django.test import TestCase


class SubdivxTestCase(TestCase):
    def setUp(self):
        return

    def test_crawl_results(self):
        filenames = [
            {
                "filename": "Modern.Family.S04E04.HDTV.x264-LOL"
            },
            # {
            #     "filename": "Cops S25E12 720p HDTV x264-BAJSKORV"
            # },
            # {
            #     "filename": "Malibu Country S01E17 HDTV x264-LOL"
            # },
            # {
            #     "filename": "Banshee S01E10 REPACK HDTV x264-2HD"
            # },
            # {
            #     "filename": "Searching for Sugar Man 2012 720p BRrip x264 AAC-MVGroup"
            # },
            # {
            #     "filename": "Spartacus S03E07 720p HDTV x264-IMMERSE"
            # },
        ]

        for test in filenames:
            parsed_file = Parser.parse_single_file(filename=test['filename'])

            site = Subdivx()
            login = site.login(user=0, search=True, download=False)

            results = site.compute_results(
                title=parsed_file['title'],
                season=parsed_file['season'],
                episode=parsed_file['episode'],
                language=SubtitleInfo.SPANISH,
                hash_id=parsed_file['hash_id'],
                year=parsed_file['year'],
                login=login
            )

            pprint(results)

            Persistence.save_results(results,
                                     parsed_file['hash_id'],
                                     SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1],
                                     site.id()
            )

        return

    def test_download_results(self):
        filename = "Modern.Family.S04E04.HDTV.x264-LOL"
        results = [
            {
                'description': 'Subtitulos para: Modern.Family.S04E04.AFG - Lol - DIMENSION - Traducido por: Rocio190889 y Lord Avestruz .:: TheSubFactory ::',
                'extra': {'server': '7'},
                'filename': 'Subtitulos para: Modern.Family.S04E04.AFG - Lol - DIMENSION - Traducido por: Rocio190889 y Lord Avestruz .:: TheSubFactory ::.',
                'id': '333508',
                'release': 'Subtitulos para: Modern.Family.S04E04.AFG - Lol - DIMENSION - Traducido por: Rocio190889 y Lord Avestruz .:: TheSubFactory ::',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.720p.HDTV.X264-DIMENSION. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'extra': {'server': '7'},
                'filename': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.720p.HDTV.X264-DIMENSION. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'id': '308183',
                'release': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.720p.HDTV.X264-DIMENSION. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.HDTV.x264-LOL. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'extra': {'server': '7'},
                'filename': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.HDTV.x264-LOL. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'id': '308180',
                'release': 'De aRGENTeaM para Modern.Family.S04E04.The.Butlers.Escape.HDTV.x264-LOL. Traducido por: luconb(x2), Bruce-Wayne, SacredHero, facu_23',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': "Los de Substeam, sincronizados para la version WEB-DL... \r Modern.Family.S04E04.The.Butler's.Escape.720p.WEB-DL.DD5.1.H.264-CtrlHD",
                'extra': {'server': '7'},
                'filename': "Los de Substeam, sincronizados para la version WEB-DL... \r Modern.Family.S04E04.The.Butler's.Escape.720p.WEB-DL.DD5.1.H.264-CtrlHD",
                'id': '304037',
                'release': "Los de Substeam, sincronizados para la version WEB-DL... \r Modern.Family.S04E04.The.Butler's.Escape.720p.WEB-DL.DD5.1.H.264-CtrlHD",
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'Para Modern Family S04E04 HDTV XviD-AFG.. Una traduccion de Tacho8. Substeam.net',
                'extra': {'server': '7'},
                'filename': 'Para Modern Family S04E04 HDTV XviD-AFG.. Una traduccion de Tacho8. Substeam.net',
                'id': '304030',
                'release': 'Para Modern Family S04E04 HDTV XviD-AFG.. Una traduccion de Tacho8. Substeam.net',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'on para Modern.Family.S04E04.HDTV.x264-LOL  espanol latino',
                'extra': {'server': '7'},
                'filename': 'son para Modern.Family.S04E04.HDTV.x264-LOL  espanol latino',
                'id': '304023',
                'release': 'on para Modern.Family.S04E04.HDTV.x264-LOL  espanol latino',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'Para Modern.Family.S04E04.HDTV.x264-LOL. Una traduccion de\r Tacho8. Substeam.net. Tambien funciona con 720p.HDTV.X264-DIMENSION',
                'extra': {'server': '7'},
                'filename': 'Para Modern.Family.S04E04.HDTV.x264-LOL. Una traduccion de\r Tacho8. Substeam.net. Tambien funciona con 720p.HDTV.X264-DIMENSION',
                'id': '304028',
                'release': 'Para Modern.Family.S04E04.HDTV.x264-LOL. Una traduccion de\r Tacho8. Substeam.net. Tambien funciona con 720p.HDTV.X264-DIMENSION',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'},
            {
                'description': 'Son los de www.subtitulos.es para la version Modern Family S04E04 HDTV x264-LOL en espanol de espana',
                'extra': {'server': '7'},
                'filename': 'Son los de www.subtitulos.es para la version Modern Family S04E04 HDTV x264-LOL en espanol de espana.',
                'id': '303713',
                'release': 'Son los de www.subtitulos.es para la version Modern Family S04E04 HDTV x264-LOL en espanol de espana',
                'score': 0,
                'server': '7',
                'site': 'Subdivx'}]

        site = Subdivx()
        login = site.login(user=0, search=False, download=True)

        for result in results:
            link = site.get_direct_download_link(result, login=login)
            print link

        return