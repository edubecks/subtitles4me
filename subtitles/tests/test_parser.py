# coding: utf-8
from pprint import pprint
from subtitles.model.parser import Parser

__author__ = 'edubecks'

from django.test import TestCase


class ParserTestCase(TestCase):

    def setUp(self):
        return

    def test_parse_filenames(self):
        tests = """
phd-movie-720p
the 4400
                """

        for test in tests.split('\n'):
            test = test.strip()
            if test:
                result = Parser.parse_single_file(test.strip())
                pprint(result)


        return