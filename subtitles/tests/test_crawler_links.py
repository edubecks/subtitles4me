# coding: utf-8
from subtitles.model.crawler.crawler import Crawler
from subtitles.model.sub_controller import SubController
from subtitles.models import Subtitle

__author__ = 'edubecks'

from django.test import TestCase


class CrawlerLinksTestCase(TestCase):
    def setUp(self):
        Subtitle(
            hash_id='1600 penn;1;6;',
            id='2262791',
            site='Podnapisi',
            description='1600.Penn.S01E06.480p.WEB-DL.x264-mSD 1600.Penn.S01E06.WEB-DL.x264-SHO',
            download_link='',
            language_iso_639_1='pb').save()
        print Subtitle.objects.count()

        return

    def test_cloudstorage(self):
        print Subtitle.objects.count()
        Crawler.crawl_links(user=0, num_threads=1, limit=3)
        download_link = SubController.download_id_subtitle(1)
        print(download_link)

        return