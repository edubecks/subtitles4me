# coding: utf-8
from pprint import pprint
from subtitles.model.crawler.crawler import Crawler
from subtitles.model.parser import Parser
from subtitles.model.storage.googlecloud import GoogleCloudStorage
from subtitles.model.sub_controller import SubController

__author__ = 'edubecks'

from django.test import TestCase
from subtitles.model.sites.legendaszone.legendaszone import *
from subtitles.model.subtitle_info import SubtitleInfo


class LegendasZoneTestCase(TestCase):

    def test_search_results(self):

        ## filename
        filename = "baby blues"
        parsed_file = Parser.parse_single_file(filename=filename)

        site = LegendasZone()
        login = site.login(user=0, search=True, download=False)

        results = site.compute_results(
            title=parsed_file['title'],
            season=parsed_file['season'],
            episode=parsed_file['episode'],
            language=SubtitleInfo.PORTUGUESE_BR,
            hash_id=parsed_file['hash_id'],
            year=parsed_file['year'],
            login=login
        )

        pprint(results)
        return

    def test_download_result(self):

        subtitle_id = '107845'

        site = LegendasZone()
        login = site.login(user=0, search=False, download=True)

        response = site.get_direct_download_link(subtitle_id, login)
        self.assertIsNotNone(response['file'], 'cannot download file')
        pprint(response)
        return

    def test_upload_to_cloud(self):
        subtitle_id = '107845'

        site = LegendasZone()
        login = site.login(user=0, search=False, download=True)

        response = site.get_direct_download_link(subtitle_id, login)
        print response

        storage = GoogleCloudStorage()
        storage.open_connection()
        url = storage.store('test', response)
        print url
        storage.close_connection()

        return

    def test_sub_controller(self):
        sites = [LegendasZone.NAME]
        portuguese_br = SubtitleInfo.PORTUGUESE_BR[SubtitleInfo.iso_639_1]
        # portuguese_por = SubtitleInfo.PORTUGUESE[SubtitleInfo.iso_639_1]
        languages = [portuguese_br]

        controller = SubController(languages=languages,
                                   sites=sites,
                                   search=True,
                                   download=False
        )

        filename = "Modern.Family.S04E04.HDTV.x264-LOL"
        filename = "How.I.Met.Your.Mother.S08E22.HDTV.x264-LOL.[VTV].mp4"

        ## analyzing filename
        parsed_file = Parser.parse_single_file(filename=filename)

        ## crawling
        controller.crawler_results(parsed_file)

        ## getting subtitles
        Crawler.crawl_links(user=0, num_threads=1, limit=30, cloud_storage=False)

        results = controller.compute_results_single(parsed_file, max_results=20)

        ## spanish results
        self.assertIsNot(
            results[portuguese_br]['results'],
            [],
            'Results not found!')
        pprint(results)

        return