# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ResultsUpdater'
        db.create_table('subtitles_resultsupdater', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hash_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('language_iso_639_1', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 1, 25, 0, 0), blank=True)),
        ))
        db.send_create_signal('subtitles', ['ResultsUpdater'])

        # Adding field 'Subtitle.download_count'
        db.add_column('subtitles_subtitle', 'download_count',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Subtitle.positive_score'
        db.add_column('subtitles_subtitle', 'positive_score',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Subtitle.negative_score'
        db.add_column('subtitles_subtitle', 'negative_score',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'ResultsUpdater'
        db.delete_table('subtitles_resultsupdater')

        # Deleting field 'Subtitle.download_count'
        db.delete_column('subtitles_subtitle', 'download_count')

        # Deleting field 'Subtitle.positive_score'
        db.delete_column('subtitles_subtitle', 'positive_score')

        # Deleting field 'Subtitle.negative_score'
        db.delete_column('subtitles_subtitle', 'negative_score')


    models = {
        'subtitles.downloadstatistics': {
            'Meta': {'object_name': 'DownloadStatistics'},
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 1, 25, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'subtitles.resultsupdater': {
            'Meta': {'object_name': 'ResultsUpdater'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 1, 25, 0, 0)', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'subtitles.subtitle': {
            'Meta': {'unique_together': "(('id', 'site', 'hash_id', 'language_iso_639_1'),)", 'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']