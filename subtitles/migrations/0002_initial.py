# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Log'
        db.create_table('subtitles_log', (
            ('id_caller', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 11, 28, 0, 0), blank=True)),
            ('caller', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('subtitles', ['Log'])

        # Adding model 'Subtitle'
        db.create_table('subtitles_subtitle', (
            ('id_subtitle', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hash_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('site', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('download_link', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('language_iso_639_1', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('extra', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('score', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('subtitles', ['Subtitle'])

        # Adding model 'DownloadStatistics'
        db.create_table('subtitles_downloadstatistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subtitle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subtitles.Subtitle'])),
            ('download_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('positive_score', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('negative_score', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('subtitles', ['DownloadStatistics'])


    def backwards(self, orm):
        # Deleting model 'Log'
        db.delete_table('subtitles_log')

        # Deleting model 'Subtitle'
        db.delete_table('subtitles_subtitle')

        # Deleting model 'DownloadStatistics'
        db.delete_table('subtitles_downloadstatistics')


    models = {
        'subtitles.downloadstatistics': {
            'Meta': {'object_name': 'DownloadStatistics'},
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 11, 28, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'subtitles.subtitle': {
            'Meta': {'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']