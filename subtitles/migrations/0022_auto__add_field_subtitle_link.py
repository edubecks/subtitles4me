# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Subtitle.link'
        db.add_column('subtitles_subtitle', 'link',
                      self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Subtitle.link'
        db.delete_column('subtitles_subtitle', 'link')


    models = {
        'subtitles.analyticsmodel': {
            'Meta': {'object_name': 'AnalyticsModel'},
            'counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        'subtitles.analyticssearch': {
            'Meta': {'object_name': 'AnalyticsSearch'},
            'counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 27, 0, 0)', 'blank': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'has_results': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 27, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        },
        'subtitles.resultsqueue': {
            'Meta': {'object_name': 'ResultsQueue'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 27, 0, 0)', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '200', 'primary_key': 'True'})
        },
        'subtitles.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 5, 27, 0, 0)', 'blank': 'True'}),
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"}),
            'score_counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'score_negative': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'score_positive': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'subtitles.subtitle': {
            'Meta': {'unique_together': "(('id', 'site', 'hash_id', 'language_iso_639_1'),)", 'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']