# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Subtitle.hash_id'
        db.alter_column('subtitles_subtitle', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=150))

    def backwards(self, orm):

        # Changing field 'Subtitle.hash_id'
        db.alter_column('subtitles_subtitle', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'subtitles.downloadstatistics': {
            'Meta': {'object_name': 'DownloadStatistics'},
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 12, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'subtitles.resultsqueue': {
            'Meta': {'object_name': 'ResultsQueue'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 12, 0, 0)', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        'subtitles.subtitle': {
            'Meta': {'unique_together': "(('id', 'site', 'hash_id', 'language_iso_639_1'),)", 'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'score': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']