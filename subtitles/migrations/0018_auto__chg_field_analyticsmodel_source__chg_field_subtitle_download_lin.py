# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'AnalyticsModel.source'
        db.alter_column('subtitles_analyticsmodel', 'source', self.gf('django.db.models.fields.CharField')(max_length=100, primary_key=True))

        # Changing field 'Subtitle.download_link'
        db.alter_column('subtitles_subtitle', 'download_link', self.gf('django.db.models.fields.CharField')(max_length=150, null=True))

        # Changing field 'Subtitle.hash_id'
        db.alter_column('subtitles_subtitle', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=100))

    def backwards(self, orm):

        # Changing field 'AnalyticsModel.source'
        db.alter_column('subtitles_analyticsmodel', 'source', self.gf('django.db.models.fields.CharField')(max_length=200, primary_key=True))

        # Changing field 'Subtitle.download_link'
        db.alter_column('subtitles_subtitle', 'download_link', self.gf('django.db.models.fields.CharField')(max_length=200, null=True))

        # Changing field 'Subtitle.hash_id'
        db.alter_column('subtitles_subtitle', 'hash_id', self.gf('django.db.models.fields.CharField')(max_length=200))

    models = {
        'subtitles.analyticsmodel': {
            'Meta': {'object_name': 'AnalyticsModel'},
            'counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        'subtitles.downloadstatistics': {
            'Meta': {'object_name': 'DownloadStatistics'},
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 27, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        },
        'subtitles.resultsqueue': {
            'Meta': {'object_name': 'ResultsQueue'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 27, 0, 0)', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '200', 'primary_key': 'True'})
        },
        'subtitles.subtitle': {
            'Meta': {'unique_together': "(('id', 'site', 'hash_id', 'language_iso_639_1'),)", 'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'negative_score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'positive_score': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']