# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'DownloadStatistics'
        db.delete_table('subtitles_downloadstatistics')

        # Adding model 'Statistics'
        db.create_table('subtitles_statistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subtitle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subtitles.Subtitle'])),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 3, 15, 0, 0), blank=True)),
            ('download_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('score_positive', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('score_counter', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('score_negative', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('subtitles', ['Statistics'])

        # Deleting field 'Subtitle.negative_score'
        db.delete_column('subtitles_subtitle', 'negative_score')

        # Deleting field 'Subtitle.positive_score'
        db.delete_column('subtitles_subtitle', 'positive_score')

        # Deleting field 'Subtitle.download_count'
        db.delete_column('subtitles_subtitle', 'download_count')


    def backwards(self, orm):
        # Adding model 'DownloadStatistics'
        db.create_table('subtitles_downloadstatistics', (
            ('subtitle', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subtitles.Subtitle'])),
            ('negative_score', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('positive_score', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('download_count', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('subtitles', ['DownloadStatistics'])

        # Deleting model 'Statistics'
        db.delete_table('subtitles_statistics')

        # Adding field 'Subtitle.negative_score'
        db.add_column('subtitles_subtitle', 'negative_score',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Subtitle.positive_score'
        db.add_column('subtitles_subtitle', 'positive_score',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Subtitle.download_count'
        db.add_column('subtitles_subtitle', 'download_count',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)


    models = {
        'subtitles.analyticsmodel': {
            'Meta': {'object_name': 'AnalyticsModel'},
            'counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        'subtitles.log': {
            'Meta': {'object_name': 'Log'},
            'caller': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 3, 15, 0, 0)', 'blank': 'True'}),
            'id_caller': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '400'})
        },
        'subtitles.resultsqueue': {
            'Meta': {'object_name': 'ResultsQueue'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 3, 15, 0, 0)', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '200', 'primary_key': 'True'})
        },
        'subtitles.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 3, 15, 0, 0)', 'blank': 'True'}),
            'download_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'score_counter': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'score_negative': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'score_positive': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'subtitle': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['subtitles.Subtitle']"})
        },
        'subtitles.subtitle': {
            'Meta': {'unique_together': "(('id', 'site', 'hash_id', 'language_iso_639_1'),)", 'object_name': 'Subtitle'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'download_link': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id_subtitle': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_iso_639_1': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'site': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['subtitles']