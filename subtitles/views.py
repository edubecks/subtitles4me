# Create your views here.

from django.shortcuts import render_to_response, render
from django.http import *
from django.core.context_processors import csrf
from django.utils import simplejson as json
from django.template import RequestContext
from unidecode import unidecode
from django.conf import settings
from subtitles.model.analytics import Analytics

from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.config import *
from subtitles.model.sub_controller import SubController
from subtitles.model.sites.registered_sites import *
from subtitles.models import AnalyticsSearch
from subtitles.views_helper_fn import ad_placer

from ratelimit.decorators import ratelimit

from django.conf import settings

DEBUG = settings.DEBUG

SITES = [Podnapisi.NAME, Subdivx.NAME, LegendasZone.NAME]
# SITES = [Podnapisi.NAME, Subdivx.NAME, LegendasTV.NAME, LegendasZone.NAME]
LANGUAGES = ['es', 'pb', 'en']


@ratelimit(ip=True, method=None, rate='25/d')
def download_by_id(request, id_subtitle):
    """
    it retrieves the download link for a given id
     returns a redirect to that link to stay in the page while downloading
    """
    was_limited = getattr(request, 'limited', False)
    if not was_limited:
        direct_download_link = SubController.download_id_subtitle(id_subtitle)
        response_data = {
            'status': 1,
            'download_link': direct_download_link
        }
    else:
        response_data = {
            'status': 0
        }
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def download(request, site, id):
    direct_download_link = SubController.download(site, id, '')
    if direct_download_link:
        return HttpResponse(direct_download_link, mimetype='application/force-download')
    return HttpResponse(status=404)


def index(request):

    languages = SubtitleInfo.LANGUAGES

    data = RequestContext(request, {
        'languages': languages,
        'title': 'Subtitles4me by edubecks',
        'sites': REGISTERED_SITES.iterkeys(),
        'filename': 'Game.of.Thrones.S03E02.720p.HDTV.x264-IMMERSE.[PublicHD]'
    })
    return render_to_response('index.html', data)


def search_filename(request, filename):
    """
    search and automatically shows results for url like
    http://127.0.0.1:8000/search/How.I.Met.Your.Mother.S07E19.HDTV.XviD-2HD.avi
    it performs the search using all the languages
    """
    languages = ['es', 'pb', 'en']
    sub_controller = SubController(languages, SITES)
    results = sub_controller.compute_results_single(filename, parsed=False)
    print filename
    info = {
        'filename': filename
    }
    return _search(request, 'results_from_search.html',results, info)



def _search(request, template, results, info=None):
    results = dict(results)


    if results:
        ads = ad_placer(results)
        data = RequestContext(request, {
            'results': results,
            'ads': ads,
            'languages':SubtitleInfo.LANGUAGES
        })
        data.update(info)
        data.update(csrf(request))
        return render_to_response(template, data)
    else:
        return HttpResponse(status=404)



def opensearch(request):
    return render(request, 'opensearch.xml', content_type="application/xhtml+xml")


def search(request):

    ## retrieve parameters from ajax request
    languages = request.GET.getlist('languages')
    list_of_files = request.GET.get('filename')
    list_of_files = list_of_files.split('\n')
    ## remove empty strings
    list_of_files = filter(None, list_of_files)

    sub_controller = SubController(languages, SITES)


    if len(list_of_files) > 1:
        results = sub_controller.compute_results_multiple(list_of_files, parsed=False)
    else:
        results = sub_controller.compute_results_single(list_of_files[0], parsed=False)


    ## django cant iterate python defauldict
    results = dict(results)

    if results:
        ads = ad_placer(results)
        data = RequestContext(request, {
            'results': results,
            'ads': ads
        })
        data.update(csrf(request))
        return render_to_response('results.html', data)
    else:
        return HttpResponse(status=404)





def about(request):
    data = RequestContext(request, {
        'title': 'about Subtitles4me',
    })
    return render_to_response('about.html', data)


def how_to_use(request):
    data = RequestContext(request, {
        'title': 'Subtitles4me by edubecks',
    })
    return render_to_response('how_to_use.html', data)


def contact(request):
    data = RequestContext(request, {
        'title': 'Subtitles4me by edubecks',
    })
    return render_to_response('contact.html', data)

def last_results(request):
    data = RequestContext(request, {
        'num_results': '20',
        'last_subtitles': Analytics.get_last_subtitles(num_results=50)
    })
    return render_to_response('last_results.html', data)





def search_and_download(request, filename, download_type='json'):

    sub_controller = SubController(LANGUAGES, SITES)
    results = sub_controller.compute_results_single(filename, parsed=False)

    ## info for download
    response_data = {
        'filename': filename
    }

    for language in LANGUAGES:
        if results[language]:
            best_result = results[language]['results'][0]
            id = best_result['id']
            site = best_result['site']
            response_data[language] = SubController.download(site, id, '')

    return HttpResponse(json.dumps(response_data), content_type="application/json")

