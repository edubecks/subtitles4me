__author__ = 'edubecks'
from django.core.management.base import BaseCommand

from subtitles.model.crawler.crawler import Crawler


class Command(BaseCommand):
    help = 'fetch new subtitles using rss'

    def handle(self, *args, **options):
#        Crawler.eztv_update_shows_list()
#        Crawler.eztv_populate_db()
        Crawler.piratebay_populate_db()
        return
