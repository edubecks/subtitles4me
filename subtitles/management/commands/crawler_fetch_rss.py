__author__ = 'edubecks'
from django.core.management.base import BaseCommand

from subtitles.model.crawler.crawler import Crawler


class Command(BaseCommand):
    help = 'fetch new subtitles using rss'

    def handle(self, *args, **options):
        Crawler.piratebay_enqueue_rss()
        Crawler.eztv_enqueue_rss()
        return
