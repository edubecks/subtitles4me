__author__ = 'edubecks'
from optparse import make_option

from django.core.management.base import BaseCommand

from subtitles.model.crawler.crawler import Crawler


class Command(BaseCommand):
    help = 'fetch new subtitles using rss'

    option_list = BaseCommand.option_list + (
        make_option('-t',
                    '--num_threads',
                    action='store',
                    dest='num_threads',
                    default=1,
                    help='specify number of threads'),
        make_option('-r',
                    '--range_hours',
                    action='store',
                    dest='range_hours',
                    default = 72,
                    help='specify number of recent hours to crawl results in pending links'),
    )

    def handle(self, *args, **options):
        num_threads  =  int(options.get('num_threads'))
        range_hours =  int(options.get('range_hours'))
        Crawler.crawl_results(num_threads, range_hours)
        return
