from optparse import make_option

__author__ = 'edubecks'
from subtitles.model.crawler.crawler import Crawler
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'crawl and fetch missing links'

    option_list = BaseCommand.option_list + (
        make_option('-t',
                    '--num_threads',
                    action='store',
                    dest='num_threads',
                    default=1,
                    help='specify number of threads'),
        make_option('-u',
                    '--user',
                    action='store',
                    dest='user',
                    default=0,
                    help='select user'),
        make_option('-l',
                    '--limit',
                    action='store',
                    dest='limit',
                    default=100,
                    help='set limit'),

        )

    def handle(self, *args, **options):
        num_threads = int(options.get('num_threads'))
        user = int(options.get('user'))
        limit = int(options.get('limit'))
        Crawler.crawl_links(user, num_threads, limit)
        return
