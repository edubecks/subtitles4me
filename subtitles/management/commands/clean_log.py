# coding: utf-8
from optparse import make_option

__author__ = 'edubecks'

from django.core.management.base import BaseCommand
from subtitles.model.log import clean_log


class Command(BaseCommand):
    help = 'clean older logs'

    option_list = BaseCommand.option_list + (
        make_option('-r',
                    '--range_hours',
                    action='store',
                    dest='range_hours',
                    help='specify number of recent hours to crawl results in pending links'),
        )

    def handle(self, *args, **options):
        if not options.get('range_hours'):
            range_hours = 336 ## 2 weeks: 24h x 14 days
        else:
            range_hours = int(options.get('range_hours'))
        clean_log(range_hours)
        return

