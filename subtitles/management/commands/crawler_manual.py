from optparse import make_option
from subtitles.model.parser import Parser
from subtitles.model.sub_controller import SubController

__author__ = 'edubecks'
from django.core.management.base import BaseCommand

from subtitles.model.crawler.crawler import Crawler


class Command(BaseCommand):
    help = 'fetch new subtitles from file'

    option_list = BaseCommand.option_list + (
        make_option('-t',
                    '--num_threads',
                    action='store',
                    dest='num_threads',
                    default=1,
                    help='specify number of threads'),
        make_option('-u',
                    '--user',
                    action='store',
                    dest='user',
                    default=0,
                    help='select user'),
        make_option('-l',
                    '--limit',
                    action='store',
                    dest='limit',
                    default=100,
                    help='set limit'),
        make_option('-f',
                    '--file',
                    action='store',
                    dest='file',
                    default=None,
                    help='set limit'),
        )


    def handle(self, *args, **options):
        num_threads = int(options.get('num_threads'))
        user = int(options.get('user'))
        limit = int(options.get('limit'))
        file = options.get('file')

        LANGUAGES = ['es', 'pb', 'en']
        SITES = ['Podnapisi', 'Subdivx', 'Legendas TV']
        sub_controller = SubController(LANGUAGES, SITES, search=True)

        if not file:
            return


        file = open(file)
        for filename in file.readlines():
            sub_controller.enqueue_results(filename)

        range_hours = 1
        Crawler.crawl_results(num_threads, range_hours)
        Crawler.crawl_links(10, num_threads, limit)
        Crawler.crawl_links(11, num_threads, limit)
        Crawler.crawl_links(12, num_threads, limit)

        return
