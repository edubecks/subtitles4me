from optparse import make_option

from subtitles.model.parser import Parser
from subtitles.model.persistence import Persistence
from subtitles.model.sub_controller import SubController
import maintenance_tasks

__author__ = 'edubecks'
from django.core.management.base import BaseCommand

class Command(BaseCommand):

    help = 'maintenance'

    option_list = BaseCommand.option_list + (
        make_option('-t',
                    '--task',
                    action='store',
                    dest='task',
                    default=None,
                    help='specify a task'),
    )

    def handle(self, *args, **options):

        tasks = {
            '1': maintenance_tasks.update_links_legendastv,
            '2': maintenance_tasks.update_links_subdivx,
            '3': maintenance_tasks.update_links_podnapisi,
            '4': maintenance_tasks.add_download_statistics,
            '5': maintenance_tasks.create_superuser,
            '6': maintenance_tasks.crawl_manual,
            '7': maintenance_tasks.create_site,
            '8': maintenance_tasks.update_analytics_search_if_results,
            '9': maintenance_tasks.remove_broken_links,
            '0': maintenance_tasks.simple_one_time_maintenance_task,
            'a': maintenance_tasks.upload_to_amazons3,
            'b': maintenance_tasks.upload_to_google_cloud_storage,

        }

        num_task = options.get('task')
        if num_task:
            task = tasks[num_task]
            print('executing', task.__name__)
            task()

        return
