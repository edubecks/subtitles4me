# coding: utf-8
__author__ = 'edubecks'

from subtitles.models import Subtitle
from subtitles.model.statistics_controller import StatisticsController

def add_download_statistics():

    subtitles = Subtitle.objects.all()
    for i, subtitle in enumerate(subtitles):
        id_subtitle = subtitle.id_subtitle
        StatisticsController.create(id_subtitle)
    return
