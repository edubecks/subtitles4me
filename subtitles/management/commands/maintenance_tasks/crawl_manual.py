# coding: utf-8
from subtitles.model.parser import Parser
from subtitles.model.sites.legendaszone.legendaszone import LegendasZone
from subtitles.model.sites.podnapisi.podnapisi import Podnapisi
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.sub_controller import SubController

__author__ = 'edubecks'


def crawl_manual():
    LANGUAGES = ['es', 'pb', 'en']
    SITES = [Podnapisi.NAME, Subdivx.NAME, LegendasZone.NAME]

    list_of_files = """
it&#039 s always sunny in philadelphia season 1 complete
    """

    sub_controller = SubController(LANGUAGES, SITES, search=True)
    for a_file in list_of_files.split('\n'):
        a_file = a_file.strip()
        if a_file:
            print('enqueing', a_file)
            sub_controller.enqueue_results(a_file)

