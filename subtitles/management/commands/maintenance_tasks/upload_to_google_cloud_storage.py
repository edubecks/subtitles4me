# coding: utf-8
import urllib2

__author__ = 'edubecks'

from subtitles.models import Subtitle
from subtitles.model.storage.googlecloud import GoogleCloudStorage

def upload_to_google_cloud_storage():

    ## google cloud storage s3 connection
    cloud = GoogleCloudStorage()
    cloud.open_connection()

    subtitles = Subtitle.objects.filter(
        link__isnull=True,
        download_link__isnull=False,
        site='Subdivx')[00000:10000]

    for i, subtitle in enumerate(subtitles):

        if (
            (not subtitle.download_link.startswith('broken_'))
            and subtitle.download_link
        ):
            print(i, subtitle.id_subtitle, subtitle.hash_id ,subtitle.download_link)
            try:
                link = urllib2.urlopen(subtitle.download_link).geturl()
                new_link = cloud.store(subtitle.id, link)
                subtitle.link = new_link
                print(subtitle.link)

            ## parentheses catch multiple exceptions
            except (urllib2.HTTPError, urllib2.URLError):
                subtitle.download_link = 'broken_'+subtitle.download_link
                print('---BROKEN-->',subtitle.download_link)
            subtitle.save()

    cloud.close_connection()

    return
