# coding: utf-8
import re
import urllib2
from subtitles.model.crawler.crawler import Crawler
from subtitles.model.persistence import Persistence
from subtitles.model.sites.legendaszone.legendaszone import LegendasZone
from subtitles.model.sites.podnapisi.podnapisi import Podnapisi
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.storage.googlecloud import GoogleCloudStorage
from subtitles.model.sub_controller import SubController
from subtitles.models import Subtitle

__author__ = 'edubecks'

LANGUAGES = ['es', 'pb', 'en']
SITES = [LegendasZone.NAME]

def simple_one_time_maintenance_task():

    GoogleCloudStorage()
    # Crawler.piratebay_enqueue_rss()
    # Crawler.eztv_enqueue_rss()
    # Crawler.crawl_links(user=0, num_threads=1, limit=3, cloud_storage=True)
    # Crawler.crawl_results(num_threads=1, within_hours=2)
    return