# coding: utf-8
__author__ = 'edubecks'

from django.contrib.auth import models as auth_models
from django.contrib.sites.models import Site

def create_superuser():

    auth_models.User.objects.create_superuser('subtitles4me',
                                              'subtitles4me@gmail.com', '1{=7x8049AO2m&j5q[|J')


def create_site():

    new_site = Site.objects.create(domain='subtitles4.me', name='subtitles4.me')
    print new_site.id