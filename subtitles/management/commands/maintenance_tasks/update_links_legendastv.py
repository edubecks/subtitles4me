# coding: utf-8
import urllib2

__author__ = 'edubecks'

from subtitles.models import Subtitle
from subtitles.model.sites.legendastv.legendastv_service import LegendasLogin


def update_links_legendastv():


    ## logging in
    login = LegendasLogin(user=1)
    print login

    subtitles = Subtitle.objects.filter(download_link__startswith='http://legendas.tv/info.php?')
    remaining = len(subtitles)
    for i, subtitle in enumerate(subtitles):
        direct_download_link = subtitle.download_link
        print i, direct_download_link, 'remaining', remaining - i
        try:
            direct_download_link = urllib2.urlopen(direct_download_link).geturl()
        except:
            direct_download_link = 'broken_' + direct_download_link
        if direct_download_link.startswith('http://legendas.tv/info.php'):
            direct_download_link = 'broken_' + direct_download_link
        print direct_download_link
        subtitle.download_link = direct_download_link
        subtitle.save()
    return