# coding: utf-8
__author__ = 'edubecks'
from subtitles.models import Subtitle

def remove_broken_links():

    ## broken subtitles
    subtitles = Subtitle.objects.filter(download_link__startswith='broken_')
    for subtitle in subtitles:
        ## clean statistics
        print 'Removing  broken', subtitle.id_subtitle, subtitle.id, subtitle.download_link
        subtitle._meta.get_all_related_objects()
        # Statistics.objects.get(pk=subtitle.id_subtitle).delete()

        subtitle.delete()

    ## locked in podnapisi
    subtitles = Subtitle.objects.filter(download_link__startswith='locked_')
    for subtitle in subtitles:
        ## clean statistics
        print 'Removing  broken', subtitle.id_subtitle, subtitle.id, subtitle.download_link
        subtitle._meta.get_all_related_objects()
        # Statistics.objects.get(pk=subtitle.id_subtitle).delete()

        subtitle.delete()

    return
