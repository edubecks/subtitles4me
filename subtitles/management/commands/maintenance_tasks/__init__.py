# coding: utf-8
__author__ = 'edubecks'

from upload_to_google_cloud_storage import upload_to_google_cloud_storage
from upload_to_amazons3 import upload_to_amazons3
from remove_broken_links import remove_broken_links
from update_analytics_search_if_results import update_analytics_search_if_results
from update_links_podnapisi import update_links_podnapisi
from update_links_legendastv import update_links_legendastv
from update_links_subdivx import update_links_subdivx
from django_admin import create_site, create_superuser
from crawl_manual import crawl_manual
from add_download_statistics import add_download_statistics
from simple_one_time_maintenance_task import simple_one_time_maintenance_task
