# coding: utf-8
__author__ = 'edubecks'

from subtitles.models import Subtitle
from subtitles.model.storage.amazons3 import S3Storage

def upload_to_amazons3():

    ## amazon s3 connection
    cloud = S3Storage()
    cloud.open_connection()

    subtitles = Subtitle.objects.filter(link__isnull=True, download_link__isnull=False)[:10]

    for subtitle in subtitles:
        new_link = cloud.store(subtitle.id, subtitle.download_link)
        subtitle.link = new_link
        print subtitle.download_link, subtitle.link
        subtitle.save()

    cloud.close_connection()

    return
