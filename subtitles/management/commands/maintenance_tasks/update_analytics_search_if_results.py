# coding: utf-8
__author__ = 'edubecks'


def update_analytics_search_if_results():
    """
    updating analytics search table if there are results for it
    """
    from subtitles.models import AnalyticsSearch, Subtitle

    analytics = AnalyticsSearch.objects.all().order_by('-date_added')

    for analytic in analytics:
        hash_id = analytic.hash_id
        print hash_id,
        if Subtitle.objects.filter(hash_id=hash_id).count():
            results = Subtitle.objects.filter(hash_id=hash_id)
            print [r.hash_id for r in results]
            print '----> has results'
            analytic.has_results = True
            analytic.save()
        else:
            analytic.has_results = False
            analytic.save()
    return
