# coding: utf-8
from collections import Set
import httplib
import multiprocessing
import urllib2
import requests
import sys
from unidecode import unidecode
from crawler.helpers import socks

__author__ = 'edubecks'

class SocksiPyConnection(httplib.HTTPConnection):
    def __init__(self, proxytype, proxyaddr, proxyport=None, rdns=True, username=None, password=None,
                 *args, **kwargs):
        self.proxyargs = (proxytype, proxyaddr, proxyport, rdns, username, password)
        httplib.HTTPConnection.__init__(self, *args, **kwargs)

    def connect(self):
        self.sock = socks.socksocket()
        self.sock.setproxy(*self.proxyargs)
        if isinstance(self.timeout, float):
            self.sock.settimeout(self.timeout)
        self.sock.connect((self.host, self.port))


class SocksiPyHandler(urllib2.HTTPHandler):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kw = kwargs
        urllib2.HTTPHandler.__init__(self)

    def http_open(self, req):
        def build(host, port=None, strict=None, timeout=0):
            conn = SocksiPyConnection(*self.args, host=host, port=port, strict=strict, timeout=timeout,
                                      **self.kw)
            return conn

        return self.do_open(build, req)

class WorkerIP(object):
    def __init__(self, id, socks_proxy_port=9050):
        super(WorkerIP, self).__init__()
        self.socks_proxy_port = socks_proxy_port + id
        self.opener = urllib2.build_opener(
            SocksiPyHandler(socks.PROXY_TYPE_SOCKS4, 'localhost', self.socks_proxy_port))
        print('worker ip in port ' + str(self.socks_proxy_port))

    def get_url(self, url):
    #        print('getting url for',url)
        url = unidecode(url)
        return self.opener.open(url).geturl()

    def ip(self, alt=0):
        urls = ['http://ipaddr.me/', 'http://curlmyip.com/', 'http://www.icanhazip.com/']
        return self.opener.open(urls[alt]).read()


def update_links_podnapisi():
    socks_proxy_port = 9050
    ips_set = Set([
        # '199.48.147.42', '199.48.147.40', '185.4.227.250', '62.220.135.129', '31.172.30.1', '128.117.43.92', '96.47.226.21', '5.39.40.151', '178.32.172.126', '84.32.116.93', '88.208.90.1', '88.198.120.155', '93.189.40.230', '204.11.50.131', '209.222.8.196', '78.108.63.46', '194.132.32.42', '199.48.147.36', '37.130.227.133', '5.9.121.38', '192.95.47.70', '5.135.54.65', '5.9.145.28', '187.63.100.24', '80.237.226.74', '176.99.6.238', '178.32.212.25', '204.124.83.131', '204.124.83.132', '50.7.184.58', '198.96.155.3', '171.25.193.20', '171.25.193.21', '212.84.206.250', '87.236.194.158', '85.10.211.53', '109.163.233.194', '204.8.156.142', '94.102.53.175', '31.131.21.179', '77.247.181.165', '85.25.208.201', '192.210.135.154', '78.108.63.44', '166.70.207.2', '178.32.79.68', '173.254.216.66', '31.172.30.4', '91.213.8.236', '31.172.30.2', '31.172.30.3', '77.109.138.42'
    ])
    num_subtitles = 100


    ## thread
    def thread_process(i, num_workers):
        port = socks_proxy_port + i * num_workers
        print 'thread at port', i, ' from ', i * num_subtitles, (i + 1) * num_subtitles
        for thread_id in xrange(num_workers):
            worker = WorkerIP(thread_id, port)
            try:
                ip = worker.ip()
            except:
                ip = worker.ip(alt=1)

            print ip
            if ip in ips_set:
                continue

            ## anhadiendo en caso sea nueva ip
            ips_set.add(ip)

            def is_locked(url):
                user_agent = {
                    'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 '
                                  '(KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17'}
                html = requests.put(url, headers=user_agent).text
                LOCKED = 'div class="form_content locked">'
                if LOCKED in html:
                    return True
                else:
                    return False

            from subtitles.models import Subtitle

            subtitles = Subtitle.objects.filter(
                download_link__startswith='http://www.podnapisi.net/en/')[
                        i * num_subtitles:(i + 1) * num_subtitles]
            remaining = len(subtitles)
            for i, subtitle in enumerate(subtitles):
                direct_download_link = subtitle.download_link
                #                    print i, direct_download_link, 'remaining', remaining - i
                #            direct_download_link = urllib2.urlopen(direct_download_link).geturl()

                ## using a worker
                try:
                    direct_download_link = worker.get_url(direct_download_link)
                    if not direct_download_link.startswith('http://www.podnapisi.net/static/'):
                        if is_locked(direct_download_link):
                            direct_download_link = 'locked_' + direct_download_link
                        else:
                            print 'banned', ip
                            break
                            #                    print direct_download_link
                    subtitle.download_link = direct_download_link
                    subtitle.save()
                except urllib2.HTTPError:
                    direct_download_link = 'broken_' + direct_download_link
                    print direct_download_link
                    subtitle.download_link = direct_download_link
                    subtitle.save()
                    continue
                except:
                    print 'exception', sys.exc_info()
                    print direct_download_link
                    continue

            print ips_set
        return


    num_workers = 10
    num_threads = 4

    if num_threads == 1:
        thread_process(0, num_workers)

    else:

        jobs = []
        for i in xrange(num_threads):
            job = multiprocessing.Process(
                target=thread_process, args=(i, num_workers)
            )
            jobs.append(job)

        for job in jobs:
            job.start()

    print ips_set

    return