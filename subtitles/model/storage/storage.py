# coding: utf-8
import os
import urllib2

__author__ = 'edubecks'

import hashlib

class Storage(object):

    def _generate_name(self, subtitle_id):
        """
        Generate new name for an id
        :param subtitle_id: subtitle id
        :return: new name that will be used to store subtitles
        """
        m = hashlib.md5()
        m.update(str(subtitle_id))
        name = m.hexdigest()
        return name

    def open_connection(self):
        return

    def key_for_store(self):
        return


    def store(self, subtitle_id, link):

        # print('uploading', link)

        if isinstance(link, basestring):
            extension = os.path.splitext(link)[1]
            ## download file
            response = self._response_for_link(link)
        else:
            extension = link['extension']
            response = link['file']

        ## getting extension and setting up new name
        name = self.__name_for_new_file(subtitle_id, extension)


        ## bucket
        k = self.key_for_store()
        k.name = name

        ## uploading
        k.set_contents_from_string(
            response.read(), ## key
            {'Content-Type': response.info().gettype()}     ## headers
        )

        ## publicly available
        k.make_public()

        ## close
        k.close()
        return name

    def __name_for_new_file(self, subtitle_id, extension):
        name = self._generate_name(subtitle_id) + extension
        return name


    def close_connection(self):
        return


    def _response_for_link(self, link):
        request = urllib2.Request(link)
        response = urllib2.urlopen(request)
        return response
