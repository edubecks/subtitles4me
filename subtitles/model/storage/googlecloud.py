# coding: utf-8
__author__ = 'edubecks'
from boto.gs.key import Key
import boto

from subtitles.model.storage.storage import Storage

from django.conf import settings
PRODUCTION = settings.AMAZON_RDS



# URI scheme for Google Cloud Storage.
GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'
GOOGLE_CLOUD_BUCKET = 'subtitles4me' if PRODUCTION else 'subtitles4me_test'
GOOGLE_CLOUD_STORAGE_ID = 'GOOGVAUONDNA4QA3KYSV'
GOOGLE_CLOUD_STORAGE_SECRET = 'hVEre/ccMAFw8FPd36JQdQqwR8+Z+4nUIMDVOUL0'
GOOGLE_CLOUD_URL = 'http://commondatastorage.googleapis.com'





class GoogleCloudStorage(Storage):
    def open_connection(self):
        self.connection = boto.connect_gs(GOOGLE_CLOUD_STORAGE_ID, GOOGLE_CLOUD_STORAGE_SECRET)
        self.bucket = self.connection.get_bucket(GOOGLE_CLOUD_BUCKET)
        ## connection
        return

    def close_connection(self):
        self.connection.close()
        return

    def key_for_store(self):
        super(GoogleCloudStorage, self).key_for_store()
        return Key(self.bucket)


    def store(self, subtitle_id, link):
        name = super(GoogleCloudStorage, self).store(subtitle_id, link)
        return GOOGLE_CLOUD_URL + '/' + GOOGLE_CLOUD_BUCKET + '/' + name
