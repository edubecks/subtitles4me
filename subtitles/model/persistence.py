#from django.db import transaction, DatabaseError
from itertools import chain
from django.db import transaction, DatabaseError
from django.utils.timezone import utc
from subtitles.model.log import log

__author__ = 'edubecks'
from subtitles.models import Subtitle, ResultsQueue
from datetime import datetime, timedelta
from subtitles.model.parser import Parser
from statistics_controller import StatisticsController

from django.conf import settings
DEBUG = settings.DEBUG

## options
DESCRIPTION_SIZE = 500



class Persistence(object):
    @classmethod
    def exists_in_database(cls, hash_id, site, language_iso_639_1):
        if Subtitle.objects.filter(hash_id=hash_id, site=site,
                                   language_iso_639_1=language_iso_639_1).exclude(download_link='').count():

            subtitles = Subtitle.objects.filter(
                hash_id=hash_id, site=site,
                language_iso_639_1=language_iso_639_1).exclude(download_link='')
            subtitles_result = []
            for subtitle in subtitles:
                subtitle_formatted = {
                    'id_subtitle': subtitle.id_subtitle,
                    'id': subtitle.id,
                    'release': subtitle.description,
                    'site': subtitle.site,
                    'download_link': subtitle.download_link,
                    'language_iso_639_1': subtitle.language_iso_639_1,
                    'extra': subtitle.extra,

                }
                subtitles_result.append(subtitle_formatted)
            return True, subtitles_result
        else:
            return False, []

    @classmethod
    def __save_result(cls, result, hash_id, language_iso_639_1, site_id):
        try:
            subtitle = Subtitle.objects.get(
                hash_id=hash_id,
                id=result['id'],
                site=site_id,
                language_iso_639_1=language_iso_639_1,
            )
        except Subtitle.DoesNotExist:
            with transaction.commit_on_success():
                subtitle = Subtitle(
                    hash_id=hash_id,
                    id=result['id'],
                    site=site_id,
                    language_iso_639_1=language_iso_639_1,
                    extra=result['extra'],
                    download_link='',
                    description=result['description'][:DESCRIPTION_SIZE]
                )
                subtitle.save()
                id_subtitle = subtitle.id_subtitle

                ## add statistics record (date, download_count)
                StatisticsController.create(id_subtitle)

                if DEBUG: print 'saving', hash_id, site_id
        return subtitle.id_subtitle

    @classmethod
    def save_results(cls, results, hash_id, language_iso_639_1, site_id):
        for result in results:
            try:
                id_subtitle = Persistence.__save_result(result, hash_id, language_iso_639_1, site_id)
            except:
                log('not saved '+hash_id+' '+language_iso_639_1+' '+site_id)
                # id_subtitle = Persistence.__save_result(result, hash_id, language_iso_639_1, site_id)

    @classmethod
    @transaction.autocommit
    def update_download_link(cls, id_subtitle, direct_download_link):
        try:
            if DEBUG: print 'updating', id_subtitle, direct_download_link
            Subtitle.objects.select_for_update().filter(id_subtitle=id_subtitle).update(download_link=direct_download_link)
        except:
            return

    @classmethod
    @transaction.autocommit
    def update_download_cloud_link(cls, id_subtitle, cloud_link):
        Subtitle.objects.select_for_update().filter(id_subtitle=id_subtitle).update(link=cloud_link)
        return

    @classmethod
    def get_download_link(cls, site, id):
        if Subtitle.objects.filter(site=site, id=id).count():
            subtitles = Subtitle.objects.filter(site=site, id=id)
            for subtitle in subtitles:
                download_link = subtitle.download_link
                if download_link:
                    return True, download_link
        log('no download link found for '+site+' '+id)
        return False, None

    @classmethod
    def get_download_link_id_subtitle(cls, id_subtitle):
        if Subtitle.objects.filter(id_subtitle=id_subtitle).count():
            subtitles = Subtitle.objects.filter(id_subtitle=id_subtitle)
            for subtitle in subtitles:

                ##todo revisar aqui
                ## cloud link
                if subtitle.link:
                    return True, subtitle.link
                else:
                    return True, subtitle.download_link
        log('no download link found for '+str(id_subtitle))
        return False, None

    @classmethod
    def enqueue_result(cls, hash_id):
        ResultsQueue.objects.get_or_create(hash_id=hash_id)
        return

    @classmethod
    def get_pending_subtitles(cls, within_hours=1, clean=False):

        if clean:
            ## cleaning oldest subtitles
            ResultsQueue.objects.filter(
                date_added__lt=(datetime.utcnow().replace(tzinfo=utc) - timedelta(hours=within_hours))
            ).delete()

        ## retrieving only
        subtitles = ResultsQueue.objects.all().order_by('hash_id')
#        subtitles = ResultsQueue.objects.filter(hash_id__gt='warehouse 13').order_by('hash_id')
        subtitles_formatted = []
        for subtitle in subtitles:
            parsed_file = Parser.unhash(subtitle.hash_id)
            parsed_file['hash_id'] = subtitle.hash_id
            parsed_file['filename'] = subtitle.hash_id
            subtitles_formatted.append(parsed_file)
        return subtitles_formatted

    @classmethod
    def get_pending_download_links(cls, limit, sites_array):

        subtitles = []

        for site in sites_array:
            subtitles_array = Subtitle.objects.filter(link__isnull=True, site=site).order_by(
                'hash_id')[:limit]
            subtitles.extend(list(subtitles_array))


        # ## Podnapisi has a limit of downloads per day
        # subtitles_1 = Subtitle.objects.filter(download_link='', site='Podnapisi').order_by('hash_id')[:limit]
        # subtitles_2 = Subtitle.objects.filter(download_link='').exclude(site='Podnapisi').order_by('hash_id')[:limit]
        # subtitles = list(chain(subtitles_1, subtitles_2))
        # print 'pending', len(subtitles)
        return subtitles


