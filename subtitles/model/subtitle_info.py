#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Created on July 22, 2012
@author: edubecks
'''


from collections import OrderedDict

class SubtitleInfo():

    SCORE = 'score'
    RELEASE = 'release'
    LINK = 'link'
    SITE = 'site'
    FILENAME = 'filename'


    podnapisi = 'podnapisi'
    iso_639_1 = 'iso_639_1'
    iso_639_1_code = 'iso_639_1_code'
    iso_639_1_code = 'script_setting'
    name = 'name'
    id_number = 'id_number'
    iso_3166_1 = 'iso_3166_1'


    ALBANIAN= {
        name: 'Albanian',
        podnapisi : '29',
        iso_639_1 : 'sq',
        iso_639_1_code : 'alb',
        iso_3166_1 : 'al',
        id_number: 30201
    }
    ARABIC= {
        name: 'Arabic',
        podnapisi: '12',
        iso_639_1: 'ar',
        iso_639_1_code: 'ara',
        iso_3166_1 : 'sb',
        id_number: '1',
    }
    BELARUSIAN= {
        name: 'Belarusian',
        podnapisi: '0',
        iso_639_1: 'hy',
        iso_639_1_code: 'arm',
        iso_3166_1 : 'by',
        id_number: '2',
    }
    BOSNIAN= {
        name: 'Bosnian',
        podnapisi: '10',
        iso_639_1: 'bs',
        iso_639_1_code: 'bos',
        iso_3166_1 : 'ba',
        id_number: '3',
    }
    BOSNIANLATIN= {
        name: 'BosnianLatin',
        podnapisi: '10',
        iso_639_1: 'bs',
        iso_639_1_code: 'bos',
        iso_3166_1 : 'ba',
        id_number: '100',
    }
    BRAZILIAN= {
        name: 'Brazilian',
        podnapisi: '48',
        iso_639_1: 'pb',
        iso_639_1_code: 'pob',
        iso_3166_1 : 'br',
        id_number: '32',
    }
    BULGARIAN= {
        name: 'Bulgarian',
        podnapisi: '33',
        iso_639_1: 'bg',
        iso_639_1_code: 'bul',
        iso_3166_1 : 'bg',
        id_number: '4',
    }
    CATALAN= {
        name: 'Catalan',
        podnapisi: '53',
        iso_639_1: 'ca',
        iso_639_1_code: 'cat',
        iso_3166_1 : 'es',
        id_number: '5',
    }
    CHINESE= {
        name: 'Chinese',
        podnapisi: '17',
        iso_639_1: 'zh',
        iso_639_1_code: 'chi',
        iso_3166_1 : 'cn',
        id_number: '6',
    }

    CROATIAN= {
        name: 'Croatian',
        podnapisi: '38',
        iso_639_1: 'hr',
        iso_639_1_code: 'hrv',
        iso_3166_1 : 'hr',
        id_number: '7',
    }
    CZECH= {
        name: 'Czech',
        podnapisi: '7',
        iso_639_1: 'cs',
        iso_639_1_code: 'cze',
        iso_3166_1 : 'cz',
        id_number: '8',
    }
    DANISH= {
        name: 'Danish',
        podnapisi: '24',
        iso_639_1: 'da',
        iso_639_1_code: 'dan',
        iso_3166_1 : 'dk',
        id_number: '9',
    }
    DUTCH= {
        name: 'Dutch',
        podnapisi: '23',
        iso_639_1: 'nl',
        iso_639_1_code: 'dut',
        iso_3166_1 : 'nl',
        id_number: '10',
    }
    ENGLISH_UK= {
        name: 'English (UK)',
        podnapisi: '2',
        iso_639_1: 'en',
        iso_639_1_code: 'eng',
        iso_3166_1 : 'gb',
        id_number: '100',
        'legendastv': '2',
    }

    ENGLISH_US= {
        # name: 'English (US)',
        name: 'English',
        podnapisi: '2',
        iso_639_1: 'en',
        iso_639_1_code: 'eng',
        iso_3166_1 : 'us',
        id_number: '100',
        'legendastv': '2',
    }
    ESTONIAN= {
        name: 'Estonian',
        podnapisi: '20',
        iso_639_1: 'et',
        iso_639_1_code: 'est',
        iso_3166_1 : 'ee',
        id_number: '12',
    }
    FARSI= {
        name: 'Farsi',
        podnapisi: '52',
        iso_639_1: 'fa',
        iso_639_1_code: 'per',
        iso_3166_1 : 'bh',
        id_number: '13',
    }
    FINNISH= {
        name: 'Finnish',
        podnapisi: '31',
        iso_639_1: 'fi',
        iso_639_1_code: 'fin',
        iso_3166_1 : 'fi',
        id_number: '14',
    }
    FRENCH= {
        name: 'French',
        podnapisi: '8',
        iso_639_1: 'fr',
        iso_639_1_code: 'fre',
        iso_3166_1 : 'fr',
        id_number: '15',
    }
    GERMAN= {
        name: 'German',
        podnapisi: '5',
        iso_639_1: 'de',
        iso_639_1_code: 'ger',
        iso_3166_1 : 'de',
        id_number: '16',
    }
    GREEK= {
        name: 'Greek',
        podnapisi: '16',
        iso_639_1: 'el',
        iso_639_1_code: 'ell',
        iso_3166_1 : 'gr',
        id_number: '17',
    }
    HEBREW= {
        name: 'Hebrew',
        podnapisi: '22',
        iso_639_1: 'he',
        iso_639_1_code: 'heb',
        iso_3166_1 : 'il',
        id_number: '18',
    }
    HINDI= {
        name: 'Hindi',
        podnapisi: '42',
        iso_639_1: 'hi',
        iso_639_1_code: 'hin',
        iso_3166_1 : 'in',
        id_number: '19',
    }
    HUNGARIAN= {
        name: 'Hungarian',
        podnapisi: '15',
        iso_639_1: 'hu',
        iso_639_1_code: 'hun',
        iso_3166_1 : 'hu',
        id_number: '20',
    }
    ICELANDIC= {
        name: 'Icelandic',
        podnapisi: '6',
        iso_639_1: 'is',
        iso_639_1_code: 'ice',
        iso_3166_1 : 'is',
        id_number: '21',
    }
    INDONESIAN= {
        name: 'Indonesian',
        podnapisi: '0',
        iso_639_1: 'id',
        iso_639_1_code: 'ind',
        iso_3166_1 : 'id',
        id_number: '22',
    }
    ITALIAN= {
        name: 'Italian',
        podnapisi: '9',
        iso_639_1: 'it',
        iso_639_1_code: 'ita',
        iso_3166_1 : 'it',
        id_number: '23',
    }
    JAPANESE= {
        name: 'Japanese',
        podnapisi: '11',
        iso_639_1: 'ja',
        iso_639_1_code: 'jpn',
        iso_3166_1 : 'jp',
        id_number: '24',
    }
    KOREAN= {
        name: 'Korean',
        podnapisi: '4',
        iso_639_1: 'ko',
        iso_639_1_code: 'kor',
        iso_3166_1 : 'kr',
        id_number: '25',
    }
    LATVIAN= {
        name: 'Latvian',
        podnapisi: '21',
        iso_639_1: 'lv',
        iso_639_1_code: 'lav',
        iso_3166_1 : 'lv',
        id_number: '26',
    }
    LITHUANIAN= {
        name: 'Lithuanian',
        podnapisi: '0',
        iso_639_1: 'lt',
        iso_639_1_code: 'lit',
        iso_3166_1 : 'lt',
        id_number: '27',
    }
    MACEDONIAN= {
        name: 'Macedonian',
        podnapisi: '35',
        iso_639_1: 'mk',
        iso_639_1_code: 'mac',
        iso_3166_1 : 'mk',
        id_number: '28',
    }
    NORWEGIAN= {
        name: 'Norwegian',
        podnapisi: '3',
        iso_639_1: 'no',
        iso_639_1_code: 'nor',
        iso_3166_1 : 'no',
        id_number: '29',
    }
    PERSIAN= {
        name: 'Persian',
        podnapisi: '52',
        iso_639_1: 'fa',
        iso_639_1_code: 'per',
        iso_3166_1 : 'ir',
        id_number: '13',
    }
    POLISH= {
        name: 'Polish',
        podnapisi: '26',
        iso_639_1: 'pl',
        iso_639_1_code: 'pol',
        iso_3166_1 : 'pl',
        id_number: '30',
    }
    PORTUGUESE= {
        name: 'Portuguese',
        podnapisi: '32',
        iso_639_1: 'pt',
        iso_639_1_code: 'por',
        iso_3166_1 : 'pt',
        id_number: '31',
        'legendastv': '10',
    }
    PORTUGUESE_BR= {
        # name: 'Portuguese-BR',
        name: 'Portuguese',
        podnapisi: '48',
        iso_639_1: 'pb',
        iso_639_1_code: 'pob',
        iso_3166_1 : 'br',
        id_number: '32',
        'legendastv': '1',
    }
    ROMANIAN= {
        name: 'Romanian',
        podnapisi: '13',
        iso_639_1: 'ro',
        iso_639_1_code: 'rum',
        iso_3166_1 : 'ro',
        id_number: '33',
    }
    RUSSIAN= {
        name: 'Russian',
        podnapisi: '27',
        iso_639_1: 'ru',
        iso_639_1_code: 'rus',
        iso_3166_1 : 'ru',
        id_number: '34',
    }
    SERBIAN= {
        name: 'Serbian',
        podnapisi: '36',
        iso_639_1: 'sr',
        iso_639_1_code: 'scc',
        iso_3166_1 : 'rs',
        id_number: '35',
    }
    SERBIANLATIN= {
        name: 'SerbianLatin',
        podnapisi: '36',
        iso_639_1: 'sr',
        iso_639_1_code: 'scc',
        iso_3166_1 : 'rs',
        id_number: '100',
    }
    SLOVAK= {
        name: 'Slovak',
        podnapisi: '37',
        iso_639_1: 'sk',
        iso_639_1_code: 'slo',
        iso_3166_1 : 'sk',
        id_number: '36',
    }
    SLOVENIAN= {
        name: 'Slovenian',
        podnapisi: '1',
        iso_639_1: 'sl',
        iso_639_1_code: 'slv',
        iso_3166_1 : 'si',
        id_number: '37',
    }
    SPANISH= {
        name: 'Spanish',
        podnapisi: '28',
        iso_639_1: 'es',
        iso_639_1_code: 'spa',
        iso_3166_1 : 'es',
        id_number: '38',
        'legendastv': '3',
    }
    SWEDISH= {
        name: 'Swedish',
        podnapisi: '25',
        iso_639_1: 'sv',
        iso_639_1_code: 'swe',
        iso_3166_1 : 'se',
        id_number: '39',
    }
    THAI= {
        name: 'Thai',
        podnapisi: '0',
        iso_639_1: 'th',
        iso_639_1_code: 'tha',
        iso_3166_1 : 'th',
        id_number: '40',
    }
    TURKISH= {
        name: 'Turkish',
        podnapisi: '30',
        iso_639_1: 'tr',
        iso_639_1_code: 'tur',
        iso_3166_1 : 'tr',
        id_number: '41',
    }
    UKRAINIAN= {
        name: 'Ukrainian',
        podnapisi: '46',
        iso_639_1: 'uk',
        iso_639_1_code: 'ukr',
        iso_3166_1 : 'ua',
        id_number: '42',
    }
    VIETNAMESE= {
        name: 'Vietnamese',
        podnapisi: '51',
        iso_639_1: 'vi',
        iso_639_1_code: 'vie',
        iso_3166_1 : 'vn',
        id_number: '43',
    }



    LANGUAGES=OrderedDict([
#        (ALBANIAN[iso_639_1] , ALBANIAN),
#        (ARABIC[iso_639_1] , ARABIC),
#        (BELARUSIAN[iso_639_1] , BELARUSIAN),
#        (BOSNIAN[iso_639_1] , BOSNIAN),
#        (BULGARIAN[iso_639_1] , BULGARIAN),
#        (CATALAN[iso_639_1] , CATALAN),
#        (CHINESE[iso_639_1] , CHINESE),
#        (CROATIAN[iso_639_1] , CROATIAN),
#        (CZECH[iso_639_1] , CZECH),
#        (DANISH[iso_639_1] , DANISH),
#        (DUTCH[iso_639_1] , DUTCH),
#        (ENGLISH_UK[iso_639_1] , ENGLISH_UK),
        (ENGLISH_US[iso_639_1] , ENGLISH_US),
#        (ESTONIAN[iso_639_1] , ESTONIAN),
#        (FARSI[iso_639_1] , FARSI),
#        (FINNISH[iso_639_1] , FINNISH),
#        (FRENCH[iso_639_1] , FRENCH),
#        (GERMAN[iso_639_1] , GERMAN),
#        (GREEK[iso_639_1] , GREEK),
#        (HEBREW[iso_639_1] , HEBREW),
#        (HINDI[iso_639_1] , HINDI),
#        (HUNGARIAN[iso_639_1] , HUNGARIAN),
#        (ICELANDIC[iso_639_1] , ICELANDIC),
#        (INDONESIAN[iso_639_1] , INDONESIAN),
#        (ITALIAN[iso_639_1] , ITALIAN),
#        (JAPANESE[iso_639_1] , JAPANESE),
#        (KOREAN[iso_639_1] , KOREAN),
#        (LATVIAN[iso_639_1] , LATVIAN),
#        (LITHUANIAN[iso_639_1] , LITHUANIAN),
#        (MACEDONIAN[iso_639_1] , MACEDONIAN),
#        (NORWEGIAN[iso_639_1] , NORWEGIAN),
#        (PERSIAN[iso_639_1] , PERSIAN),
#        (POLISH[iso_639_1] , POLISH),
#        (PORTUGUESE[iso_639_1] , PORTUGUESE),
        (PORTUGUESE_BR[iso_639_1] , PORTUGUESE_BR),
#        (ROMANIAN[iso_639_1] , ROMANIAN),
#        (RUSSIAN[iso_639_1] , RUSSIAN),
#        (SERBIAN[iso_639_1] , SERBIAN),
#        (SLOVAK[iso_639_1] , SLOVAK),
#        (SLOVENIAN[iso_639_1] , SLOVENIAN),
        (SPANISH[iso_639_1] , SPANISH),
#        (SWEDISH[iso_639_1] , SWEDISH),
#        (THAI[iso_639_1] , THAI),
#        (TURKISH[iso_639_1] , TURKISH),
#        (UKRAINIAN[iso_639_1] , UKRAINIAN),
#        (VIETNAMESE[iso_639_1] , VIETNAMESE),
    ])

    @classmethod
    def language_translate(cls,language, language_from, language_to):
        LANGUAGE_INDEX=[SubtitleInfo.name,
                        SubtitleInfo.podnapisi,
                        SubtitleInfo.iso_639_1,
                        SubtitleInfo.iso_639_1_code,
                        SubtitleInfo.iso_639_1_code]
        for language_key, language_info in SubtitleInfo.LANGUAGES.items():
            if language == language_key :
                return language_info[LANGUAGE_INDEX[language_to]]
        return ''

    def __init__(self):
        pass


    def __str__(self):
        return self.NAME


## hack
languageTranslate = SubtitleInfo.language_translate
