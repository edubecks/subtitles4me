from __future__ import division
from django.db import connection
import time
from unidecode import unidecode
from subtitles.model.storage.googlecloud import GoogleCloudStorage

__author__ = 'edubecks'
#import os
import feedparser

import multiprocessing
from Queue import Empty as QueueEmpty
from subtitles.model.sub_controller import SubController
from subtitles.model.sites.podnapisi.podnapisi import Podnapisi
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.sites.legendastv.legendastv import LegendasTV
from subtitles.model.sites.legendaszone.legendaszone import LegendasZone
import math

from django.conf import settings

DEBUG = settings.DEBUG

LANGUAGES = ['es', 'pb', 'en']
SITES = [Podnapisi.NAME, Subdivx.NAME, LegendasZone.NAME]
# SITES = [LegendasZone.NAME]

def _chunks(l, n):
    """ Yield successive n-sized _chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


class Crawler(object):
    def __init__(self):
        super(Crawler, self).__init__()

    @classmethod
    def piratebay_enqueue_rss(cls):
        RSS_LIST = [
            'http://rss.thepiratebay.se/201',
            'http://rss.thepiratebay.se/202',
            'http://rss.thepiratebay.se/205',
            'http://rss.thepiratebay.se/207',
            'http://rss.thepiratebay.se/208',
        ]
        sub_controller = SubController(LANGUAGES, SITES)
        for rss_xml in RSS_LIST:
            rss = feedparser.parse(rss_xml)
            for entry in rss.entries:
                filename = unidecode(entry.title)
                sub_controller.enqueue_results(filename)
        return

    @classmethod
    def eztv_enqueue_rss(cls):
        RSS_LIST = [
            'http://feeds.feedburner.com/eztv-rss-atom-feeds?format=xml'
        ]
        sub_controller = SubController(LANGUAGES, SITES)
        for rss_xml in RSS_LIST:
            rss = feedparser.parse(rss_xml)
            for entry in rss.entries:
                filename = unidecode(entry.title[6:])
                sub_controller.enqueue_results(filename)
        return

    @classmethod
    def crawl_results(cls, num_threads=1, within_hours=1, sub_controller=None):

        ## getting recent subtitles
        if not sub_controller:
            sub_controller = SubController(LANGUAGES, SITES, search=True)
        subtitles = sub_controller.get_pending_subtitles(within_hours)

        ## processing queue input
        subtitles_queue = multiprocessing.Queue()
        for subtitle in subtitles:
            subtitles_queue.put(subtitle)

        ## wait for processes
        jobs = []

        # Start consumers
        for i in range(num_threads):
            p = multiprocessing.Process(
                target=Crawler.__crawl_results_from_list,
                args=(sub_controller,subtitles_queue,)
            )
            jobs.append(p)
            p.start()

        # Final process to handle the queue out
        for job in jobs:
            job.join()

        return


    @classmethod
    def __crawl_results_from_list(cls, sub_controller, subtitles_queue):

        """Put the line into the queue out"""
        while True:
            try:
                subtitle = subtitles_queue.get(timeout = 1) # Timeout after 1 second
                # print('crawling', subtitle)
                sub_controller.crawler_results(subtitle)
            except QueueEmpty:
                return # Exit when all work is done
            except:
                raise # Raise all other errors


    @classmethod
    def __crawl_links_worker(cls, sub_controller, subtitles_queue):
        """Put the line into the queue out"""
        while True:
            try:
                subtitle = subtitles_queue.get(timeout=1) # Timeout after 1 second
                # print 'crawling', subtitle.site, subtitle.id, subtitle.extra
                sub_controller.update_links(subtitle.id_subtitle,
                                            subtitle.site,
                                            subtitle.id,
                                            subtitle.extra)
            except QueueEmpty:
                return # Exit when all work is done
            except:
                raise # Raise all other errors


    @classmethod
    def crawl_links(cls, user, num_threads, limit, cloud_storage=True, sub_controller=None):

        ## setting up cloud storage
        if cloud_storage:
            cloud_storage = GoogleCloudStorage()

        ## creating subcontroller
        if not sub_controller:
            sub_controller = SubController(LANGUAGES,
                                           SITES ,
                                           user,
                                           download=True,
                                           cloud_storage=cloud_storage)
        subtitles = sub_controller.get_pending_download_links(limit, SITES)
        # print len(subtitles)


        ## processing queue input
        subtitles_queue = multiprocessing.Queue()
        for subtitle in subtitles:
            subtitles_queue.put(subtitle)

        ## wait for processes
        jobs = []

        ## closing django db connection
        # connection.close()

        # Start consumers
        for i in range(num_threads):
            p = multiprocessing.Process(
                target=Crawler.__crawl_links_worker,
                args=(sub_controller, subtitles_queue, )
            )
            jobs.append(p)
            p.start()

        # Final process to handle the queue out
        for job in jobs:
            job.join()

        # Exception in thread QueueFeederThread (most likely raised during interpreter shutdown)
        # http://mail.python.org/pipermail/python-list/2013-February/641063.html
        time.sleep(0.5)

        return


    @classmethod
    def eztv_update_shows_list(cls):
        import requests
        import re
        import os

        RSS_SHOWS = 'http://www.ezrss.it/shows/'
        REGEX_SHOW = '(/search/\?.*)&amp;'

        ## current directory
        DIRECTORY = os.path.abspath(os.path.dirname(__file__))
        EZTV_SHOWS_FILE = DIRECTORY + '/eztvshows.txt'

        shows_list = requests.get(RSS_SHOWS).text
        shows_list = re.findall(REGEX_SHOW, shows_list)
        shows_list = '\nhttp://www.ezrss.it'.join(shows_list)

        ## saving to file
        eztv_file = open(EZTV_SHOWS_FILE, 'w')
        eztv_file.write('http://www.ezrss.it')
        eztv_file.write(shows_list)
        eztv_file.close()

        return

    @classmethod
    def eztv_populate_db(cls):
        import os

        sub_controller = SubController(LANGUAGES, SITES)

        ## current directory
        DIRECTORY = os.path.abspath(os.path.dirname(__file__))
        EZTV_SHOWS_FILE = DIRECTORY + '/eztvshows.txt'

        eztv_file = open(EZTV_SHOWS_FILE, 'r')
        for url in eztv_file.readlines():
            if DEBUG: print url
            Crawler.__eztv_populate_show(sub_controller, url)
        eztv_file.close()

        return

    @classmethod
    def __eztv_populate_show(cls, sub_controller, url):
        import requests
        import re
        from unidecode import unidecode

        REGEX = '<td.*\s.*?>(.+)</a>\n(.*)</td>\n.*<td class="tc2'
        results = requests.get(url).text
        results = re.findall(REGEX, results)

        for result in results:
            filename = (' '.join(result)).replace('/', '')
            if DEBUG: print filename
            sub_controller.enqueue_results(filename)

        return

    @classmethod
    def piratebay_populate_db(cls):
        import requests
        import re
        from unidecode import unidecode


        WEB_LIST = [
            'http://thepiratebay.sx/browse/201/%d/7', ##  movies / page / sort by
            'http://thepiratebay.sx/browse/202/%d/7', ##  movies / page / sort by
            'http://thepiratebay.sx/browse/205/%d/7', ##  movies / page / sort by
            'http://thepiratebay.sx/browse/207/%d/7', ##  movies / page / sort by
            'http://thepiratebay.sx/browse/208/%d/7', ##  movies / page / sort by
        ]
        regex = re.compile(""".*="detName\".*>(.*)</a>""", re.IGNORECASE)

        sub_controller = SubController(LANGUAGES, SITES)

        user_agent = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 '
                                    '(KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17'}

        for web in WEB_LIST:
            for i in xrange(100):
                url = (web % i)
                results = requests.put(url, headers=user_agent).text
                results = re.findall(regex, results)
                for result in results:
                    # print result
                    sub_controller.enqueue_results(result)
        return

