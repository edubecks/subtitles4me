from datetime import datetime, timedelta

__author__ = 'edubecks'
import inspect
from subtitles.models import Log
from unidecode import unidecode

def log(message, caller=None):
    ## hack comentando esto porque es una limitacion en BD gratuitas-> max 10K filas (heroku)
    if not caller:
        caller = (_whoami()+'__'+_whosdaddy())
    Log(caller=caller[:100], message=unidecode(message[:200])).save()
    return


def clean_log(older_than_hours):
    Log.objects.filter(
        date_added__lt=(datetime.utcnow() - timedelta(hours=older_than_hours))
    ).delete()

    return




def _whoami():
    return inspect.stack()[1][3]

def _whosdaddy():
    return inspect.stack()[2][3]



def logger(func):
    '''Decorator to print function call details - parameters names and effective values'''

    def wrapper(*func_args, **func_kwargs):
        arg_names = func.func_code.co_varnames[:func.func_code.co_argcount]
        args = func_args[:len(arg_names)]
        defaults = func.func_defaults or ()
        args = args + defaults[
                      len(defaults) - (func.func_code.co_argcount - len(args)):]
        params = zip(arg_names, args)
        args = func_args[len(arg_names):]
        if args: params.append(('args', args))
        if func_kwargs: params.append(('kwargs', func_kwargs))
        print func.func_name + ' (' + ', '.join(
            '%s = %r' % p for p in params) + ' )'
        return func(*func_args, **func_kwargs)

    return wrapper