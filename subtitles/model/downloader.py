#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Created on July 22, 2012
@author: edubecks
"""

from __future__ import division

import re

from subtitles.model.file_downloader import FileDownloader
from subtitles.model.utilities import *


class Downloader():

    def _get_release_description(self, release):
        release_tags = re.findall(r'[\w\d]+', release)
        return [tag.lower() for tag in release_tags]

    def _compute_matching_score(self, release1, release2, site_ranking=1):

        MIN_SCORE = 33  ## 33 %

        release_tags1 = self._get_release_description(release1)

        ## no release found
        if not len(release_tags1):
            return 100 * site_ranking
        release_tags2 = self._get_release_description(release2)


        EXCLUDED_TAGS = ['xvid', 'ac3', 'x264', '1080p', '720p']

        score = 0
        for tag1 in release_tags1:
            if not tag1 in EXCLUDED_TAGS:
                for tag2 in release_tags2:
                    if tag1 == tag2:
                        score +=1
                        break
            else:
                score+=1

        score = score / len(release_tags1) * site_ranking * 100

#        score = score * 50 / len(release_tags1) + 50
#        score = score if score<=100 else 100
        return max(score, MIN_SCORE)

    def _sort_results_by_score(self, results_per_language):
        if results_per_language:
            ## sort results
            return sorted(results_per_language, key=lambda x: x['score'],reverse=True)
        else:
            return results_per_language

    def compute_score_site_results(self, release, results):
        for result in results:
            site_ranking = result['site_score']
            result['score'] = self._compute_matching_score(release, result['release'], site_ranking )



    def select_best_results(self, results_dict):
        """
        Selects best results from the the dictionary of results given,
        returns a new dictionary with the best result for each.
        """

        best_results = {}

        for language, results_per_language in results_dict.items():
            results = results_per_language['results']
            if results:
                top_result = max(results, key=lambda x: x['score'])
                best_results[language] = top_result
            else:
                best_results[language] = None


        return best_results



    def download(self, downloads):

        file_downloader = FileDownloader()

        for each_download in downloads:
            file_downloader.download_file(
                file_type=each_download[FILE_TYPE],
                link = each_download[DOWNLOAD_LINK],
                directory =  each_download[DIRECTORY],
                filename= each_download[DOWNLOAD_FILE]
            )


def argumentParser():
    parser = optparse.OptionParser()

    ## Input File
    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="input", default='some random input')

    ## Output File
    parser.add_option('-o', '--output',
                      action="store", dest="output",
                      help="output", default='output file etc ')

    ## Clipboard
    parser.add_option('-c', '--clipboard',
                      action="store", dest="clipboard",
                      help="clipboard", default='')

    ## Command
    parser.add_option('-r', '--run',
                      action="store", dest="command",
                      help="command", default='ls -l ~/Dropbox')

    ## Number
    parser.add_option("-n", type="int", dest="number")
    parser.add_option("-f", type="float", dest="float")

    ## Debug
    parser.add_option("-v", action="store_true", dest="verbose")

    options, args = parser.parse_args()

    return options


def main():

    arguments = argument_parser()

    filename = arguments.input
    filename = '/Volumes/datos/torrents/The.Walking.Dead.S03E06.HDTV.x264-ASAP.[VTV].mp4'

    LANGUAGES = ['es', 'pb', 'en']
    SITES = ['Subdivx']
    #SITES = ['Podpnapisi', 'Subdivx', 'Legendas TV']
    ## todo: registrar las peticiones
    ## circular
#    sub_controller = SubController(LANGUAGES, SITE)
#    results = sub_controller.compute_results_single(filename, use_persistence=False, parsed=False)
