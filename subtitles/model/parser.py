import os
import re
from unidecode import unidecode

__author__ = 'edubecks'

REGEX_EXPRESSIONS_MOVIES = [
    '(.*)[\s\_\-\(\.\[]+([0-9]{4})[\s\_\-\)\.\]]*(.*1080.*)',
    '(.*)[\s\_\-\(\.\[]+([0-9]{4})(.*720.*)',
    '(.*)[\s\_\-\(\.\[]+([0-9]{4})(.*1080.*)',
    '(.*)[\s\_\-\(\.\[]+([0-9]{4})(.*x264.*)',
    '(.*)[\s\_\-\(\.\[]+()(.*720.*)',
    '(.*)[\s\_\-\(\.\[]+()(.*1080.*)',
    '(.*)[\s\_\-\(\.\[]+()(.*x264.*)',
    '(.*)[\s\_\-\(\.\[]+([0-9]{4})[\s\_\-\)\.\]]*(.*)',
    '(.*)[\s\_\-\(\.]+()(UNRATED[\s\_\-\)\.]*.*)',
    '(.*)[\s\_\-\(\.]+()(DVDRip[\s\_\-\)\.]*.*)',
    '(.*)[\s\_\-\(\.]+()(bdrip[\s\_\-\)\.]*.*)',
    '(.*)[\s\_\-\(\.]+()(dvdscr[\s\_\-\)\.]*.*)',
]

REGEX_EXPRESSIONS_SHOWS_WITH_DATE= [
    '(.*[\._ \-][0-9]{4}[\._ \-][0-9]{2}[\._ \-][0-9]{2})(.*)',
]

REGEX_EXPRESSIONS_SERIES = [
    ## todo optimizar expresiones regulares
    # foo S01E01
    '(.*)[Ss]([0-9]+)[][._-]*[Ee]([0-9]+)([^\\\\/]*)$',
    #/* foo, s01e01, foo.s01.e01, foo.s01-e01*/
    '(.*)[\._ \-][Ss]([0-9]+)[\.\-\s]?[Ee]([0-9]+)([^\\/]*)',
    # foo.1x09
    '(.*)[\._ \-]([0-9]+)x([0-9]+)([^\\/]*)',
    '(.*)[\\\\/\\._ -][0]*([0-9]+)x[0]*([0-9]+)([^\\/])*',
    '(.*)[\\\\/\\._ \\[\\(-]([0-9]+)x([0-9]+)([^\\\\/]*)$'
    '(.*)[\._ \-]([0-9]+)x([0-9]+)([^\\/]*)',

    # Season - Episode
    '(.*)season\s?([0-9]+)\s?-?\s?episode\s?([0-9]+)([^\\/]*)',
    #foo_[s01]_[e01]
    '(.*)[[Ss]([0-9]+)\]_\[[Ee]([0-9]+)([^\\/]*)',
    #foo - s01ep03, foo - s1ep03
    #/* foo - s01ep03, foo - s1ep03*/
    '(.*)s([0-9]+)ep([0-9]+)([^\\/]*)',
    '(.*)[Ss]([0-9]+)[][ ._-]*[Ee]([0-9]+)([^\\\\/]*)$',
    '(.*)[\\\\/\\._ \\[\\(-]([0-9]+)x([0-9]+)([^\\\\/]*)$',
    '(.*)[Ss]([0-9]+)[\s._-]*[Ee]([0-9]+)([^\\\\/]*)$',
    #/* foo_[s01]_[e01]*/
    '(.*)[[Ss]([0-9]+)\]_\[[Ee]([0-9]+)([^\\/]*)',

    # foo.109
    '(.*)[\._ \-]([0-9])([0-9][0-9])([\._ \-][^\\/]*)'
    '(.*)[\._ \-](0[0-9])([0-9][0-9])[\._ \-]([^\\/]*)',
    '(.*)[\._ \-](1[0-8])([0-9][0-9])[\._ \-]([^\\/]*)',
#    '(.*)[\\\\/\\._ -]([0-1][0-9])([0-9][0-9])[\._ \-]([^\\/]*)',
#    '(.*)[\\\\/\\._ -]([0-1][0-9])([0-9][0-9])([^\\/]*)',
    #/*  foo.109*/
]

EXTENSIONS = ['avi', 'flv', 'mov', 'mp4', 'mpg', 'm2ts', 'mts', 'rmvb', 'avchd', 'mkv',
              'webm', 'qt', 'wmv', 'vob', '3gp', '3gpp2', 'divx']

class Parser(object):

    @classmethod
    def __get_folder_filename_extension(cls, filename):
        """
        :rtype : [string,string,string]
        Ex:
        Input:
        /Users/edubecks/Dropbox/Developer/Scripts/How.I.Met.Your.Mother.S07E09.720p.HDTV.X264-DIMENSION.avi
        Output:
        (
        /Users/edubecks/Dropbox/Developer/Scripts/ ,
        'How.I.Met.Your.Mother.S07E09.720p.HDTV.X264-DIMENSION'
        )
        """
#        filename, extension = (os.path.splitext(filename)[0], os.path.splitext(filename)[1])

        extension = ''
        for ext in EXTENSIONS:
            if filename.endswith(ext):
                filename = filename[:-(len(ext)+1)]
                extension = ext
                break
        return os.path.dirname(filename) + '/', os.path.basename(filename), extension

    @classmethod
    def hash(cls, title, season, episode, year):
        return ';'.join([title, season, episode, year])

    @classmethod
    def unhash(cls, hash_id):
        info = unidecode(hash_id).split(';')
        title, season, episode, year = info[0], info[1], info[2], info[3]
        return {
            'title': title,
            'release':'',
            'season':season,
            'episode':episode,
            'year':year
        }

    @classmethod
    def __parse(cls, file):
        file = file.strip()
        directory, filename_original, extension = Parser.__get_folder_filename_extension(file)

        ## preprocess filename (clean ad __sanitize)
        filename = filename_original.lower().strip()
        filename = Parser.__sanitize(filename)

        result = Parser.__parse_filename(filename)
        result['directory'] = directory
        result['extension'] = extension
        result['title']  = result['title'].strip()
        result['filename'] = filename_original.replace(' ','.')
        result['hash_id'] =  cls.hash(result['title'],result['season'],result['episode'], result['year'])

        ## identify release tags for future processing and similarity comparison
        result['release'] = ' '.join(sorted(re.findall(r'[\w\d]+', result['release'])))

        return result

    @classmethod
    def __sanitize(cls, text):
        for char in '[]().-_,;':
            text = text.replace(char, ' ')
        return ' '.join(text.split())

    @classmethod
    def __parse_filename(cls, filename):
        """
        Input:
        'How.I.Met.Your.Mother.S07E09.720p.HDTV.X264-DIMENSION'

        Output:
        (
        How I Met Your Mother,  # tv series
        07,                     # season
        09                      # episode
        )
        """

        ## series
        for regex in REGEX_EXPRESSIONS_SERIES:
            response_file = re.findall(regex, filename)
            if len(response_file) > 0:
                title = response_file[0][0]
                season = str(int(response_file[0][1]))
                episode = str(int(response_file[0][2]))
                release = response_file[0][3]
                return {
                    'title': title,
                    'season': season,
                    'episode': episode,
                    'release': release,
                    'year': ''
                }

        ## shows with date
        for regex in REGEX_EXPRESSIONS_SHOWS_WITH_DATE:
            response_file = re.findall(regex, filename)
            if len(response_file) > 0:
                title = response_file[0][0]
                release = response_file[0][1]
                return {
                    'title': title,
                    'season': '',
                    'episode': '',
                    'release': release,
                    'year': ''
                }


        ## title, season, episode, release
        for regex in REGEX_EXPRESSIONS_MOVIES:
            response_file = re.findall(regex, filename)
            if len(response_file) > 0:
                title = response_file[0][0]
                year = response_file[0][1]
                release = response_file[0][2]
                return {
                    'title': title,
                    'release': release,
                    'season': '',
                    'episode': '',
                    'year':year
                }


        return {
            'title': filename,
            'release': '',
            'season': '',
            'episode': '',
            'year': ''
        }

    @classmethod
    def parse_single_file(cls, filename):
        """

        :param filename: string with the filename. Ex: "Modern.Family.S04E04.HDTV.x264-LOL"
        :rtype : dict with attributes: title, release, season, episode, year
        ## parsing filename
        ##
        ## Modern.Family.S03E12.HDTV.XviD-LOL
        ##
        ## to
        ##
        ## {'directory': '/',
        ##  'episode': '12',
        ##  'extension': '',
        ##  'filename': 'Modern.Family.S03E12.HDTV.XviD-LOL',
        ##  'release': 'hdtv lol xvid',
        ##  'season': '3',
        ##  'title': 'modern family',
        ##  'year': ''}
        """
        parsed_file = Parser.__parse(file=filename)
        return parsed_file


    @classmethod
    def parse_multiple_files(cls, files):
        parsed_files = []

        ##clean
        if isinstance(files, basestring):
            CHARS_TO_STRIP = '.;, \n'
            files = files.strip(CHARS_TO_STRIP).split('\n')

        for a_file in files:
            parsed_files.append(Parser.parse_single_file(filename=a_file))
        return parsed_files


def main():
    from pprint import pprint

    tests = """phd-movie-720p
                """
    for test in tests.split('\n'):
        result = Parser.__parse(test)
        pprint(result)
    pass


if __name__ == '__main__':
    main()