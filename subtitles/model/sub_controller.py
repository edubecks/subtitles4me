# coding=utf-8
__author__ = 'edubecks'
from collections import defaultdict
import urllib2
import ast

from unidecode import unidecode
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson as json

from subtitles.model.parser import Parser
from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.downloader import Downloader
from subtitles.model.persistence import Persistence
from subtitles.model.statistics_controller import StatisticsController
from subtitles.model.sites.registered_sites import REGISTERED_SITES
from subtitles.model.log import log
from subtitles.model.analytics import Analytics

from django.conf import settings
DEBUG = settings.DEBUG


class SubController(object):
    def __init__(self, languages, sites=None, user=0, search=False, download=False, cloud_storage=None):
        """
        creates a SubController to search and download subtitles

        :param languages: list of languages
        :param sites: list of sites
        :param user: number of the user to be used when logging in some site
        :param search: boolean to define if the SubController will be used to search subtitles
        :param download:    boolean to define if the SubController will be used to download subtitles
        :param cloud_storage: Storage class to use cloud storage of the subtitles
        """
        super(SubController, self).__init__()
        self.languages = [SubtitleInfo.LANGUAGES[unidecode(language)] for language in languages]
        self.selected_sites = [REGISTERED_SITES[unidecode(site_name)] for site_name in sites]

        ## logging in for each site
        for site in self.selected_sites:
            site.login(user, search, download)

        ## saving sessions
        if search:
            self.sessions_searches = {}
            for site in self.selected_sites:
                self.sessions_searches[str(site)] = site.session_search
        if download:
            self.sessions_download = {}
            for site in self.selected_sites:
                self.sessions_download[str(site)] = site.session_download
        self.downloader = Downloader()

        ## session for cloud storage
        self.cloud_storage =  cloud_storage
        self.cloud_open_connection = False



    @classmethod
    def __htmldecode(cls, some_string):

        html_codes = [
            ('&amp aacute ', 'a'),
            ('&amp eacute ', 'e'),
            ('&amp iacute ', 'i'),
            ('&amp oacute ', 'o'),
            ('&amp uacute ', 'u'),
            ('&amp;aacute;', 'a'),
            ('&amp;eacute;', 'e'),
            ('&amp;iacute;', 'i'),
            ('&amp;oacute;', 'o'),
            ('&amp;uacute;', 'u'),
            ('&amp acirc ', 'a'),
            ('&amp ecirc ', 'e'),
            ('&amp icirc ', 'i'),
            ('&amp ocirc ', 'o'),
            ('&amp ucirc ', 'u'),
            ('&amp;uuml;', 'u'),
            ('&amp amp ', '& '),
            ('&amp;amp;', '& '),
            ('&amp lt ', ' '),
            ('&amp;lt;', ' '),
            ('\\', ' '),
            ('&amp atilde ', 'a'),
            ('&amp etilde ', 'e'),
            ('&amp otilde ', 'o'),
            ('&amp ntilde ', 'ñ'),
            ('&amp ccedil ', 'ç'),
            ('&amp;atilde;', 'a'),
            ('&amp;etilde;', 'e'),
            ('&amp;otilde;', 'o'),
            ('&amp;ntilde;', 'ñ'),
            ('&amp;ccedil;', 'ç'),

            ('&amp norwegian ', ' '),
            ('&#039 ', '\''),
            ('&#039;', '\''),
        ]

        new_string = some_string

        for code, new_code in html_codes:
            new_string = new_string.replace(code, new_code)

        return new_string

    @classmethod
    def enqueue_results(cls, filename):

        ## converting from utf-8
        # print 'enqueueing', filename
        filename = unidecode(filename.strip())

        ## cleaning html entities
        # print 'before html', filename
        filename = cls.__htmldecode(filename)
        # print 'after html', filename, '\n'

        ## parsing filename
        parsed_file = Parser.parse_single_file(filename=filename)
        title = parsed_file['title']
        season = parsed_file['season']
        episode = parsed_file['episode']
        year = parsed_file['year']
        hash_id = Parser.hash(title, season, episode, year)

        ## title must have more than 2 characters
        if len(title)>=2:

            ## adding to analytics
            Analytics.create(filename, hash_id)

            ## enqueing results
            Persistence.enqueue_result(hash_id)

        return

    def compute_results_single(self, parsed_file, max_results=10, parsed=True,
                               dict_string=False, analytics=True):

        ## parsing user data

        if not parsed:
            parsed_file = Parser.parse_single_file(parsed_file)
        elif dict_string:
            parsed_file = ast.literal_eval(unidecode(parsed_file))
        title = parsed_file['title']
        release = parsed_file['release']
        season = parsed_file['season']
        episode = parsed_file['episode']
        year = parsed_file['year']
        filename = parsed_file['filename']
        hash_id = Parser.hash(title, season, episode, year)


        ## analytics
        if analytics:
            Analytics.search_single()
            for language in self.languages:
                Analytics.source(str(language[SubtitleInfo.name]))
            for site in self.selected_sites:
                Analytics.site(site)
            Analytics.search_filename_result(filename, hash_id)

        results = {}
        ## iterating over languages
        for language in self.languages:
            language_iso_639_1 = language[SubtitleInfo.iso_639_1]

            results[language_iso_639_1] = {
                SubtitleInfo.iso_3166_1: language[SubtitleInfo.iso_3166_1],
                SubtitleInfo.name: language[SubtitleInfo.name],
                'results': []
            }
            ## iterating over sites

            for site in self.selected_sites:
                ## check if site has subtitles for the specified language
                if site.is_available_for_language(language):

                    site_score = site.score_for_language(language)

                    exists, site_results = Persistence.exists_in_database(hash_id,
                                                                          str(site),
                                                                          language_iso_639_1)


                    ## apply given score for each site
                    for result in site_results:
                        result['site_score'] = site_score

                    if not exists:
                        log('not found, enqueuing ' + filename + ' ' + hash_id)
                        self.enqueue_results(filename)

                    ## appending results retrieved from each site
                    ## compute scores for results
                    self.downloader.compute_score_site_results(release, site_results)
                    results[language_iso_639_1]['results'].extend(site_results)


            ## sorting results
            results[language_iso_639_1]['results'] = self.downloader._sort_results_by_score(
                results[language_iso_639_1]['results'])[:max_results]
            ## counter for results
            results[language_iso_639_1]['counter'] = len(results[language_iso_639_1]['results'])

        return results

    def compute_results_multiple(self, list_of_files, parsed=True):
        ## analytics
        Analytics.search_multiple()
        for language in self.languages:
            Analytics.source(str(language[SubtitleInfo.name]))
        for site in self.selected_sites:
            Analytics.site(site)


        ## parse multiline user input into several parsed_files
        parsed_files = list_of_files
        if not parsed:
            parsed_files = Parser.parse_multiple_files(list_of_files)

        results_per_language = defaultdict(lambda: defaultdict(list))


        ## counter for results, used to compute the number of ads in each table of results
        for language in self.languages:
            results_per_language[language['iso_639_1']]['counter'] = 0

        for parsed_file in parsed_files:
            not_found = {
                'site': 'NOT FOUND',
                'release': parsed_file['filename'],
                'score':None,

            }

            ## compute and choose the best result for each file
            results = self.compute_results_single(parsed_file,
                                                  parsed=True, analytics=False)
            best_results = self.downloader.select_best_results(results)

            for language, result in best_results.iteritems():
                results_per_language[language][SubtitleInfo.iso_3166_1] = (
                    SubtitleInfo.LANGUAGES[language][SubtitleInfo.iso_3166_1]  )
                results_per_language[language][SubtitleInfo.name] = SubtitleInfo.LANGUAGES[language][
                                                                    SubtitleInfo.name]
                if result:
                    results_per_language[language]['results'].append(result)

                    results_per_language[language]['counter'] += 1
                else:
                    results_per_language[language]['results'].append(not_found)

        return results_per_language



    def update_links(self, id_subtitle, site, id, extra):

        ## openning connection for cloud storage
        if self.cloud_storage and not self.cloud_open_connection:
            self.cloud_storage.open_connection()
            self.cloud_open_connection = True

        ## extra info used sometimes to retrieve the link
        result = {
            'id': id,
            'extra': ast.literal_eval(extra) if extra else None,
        }


        ## compute link for each result
        site = REGISTERED_SITES[site]
        session = site.session_download
        direct_download_link = site.get_direct_download_link(result, login=session)
        if not direct_download_link:
            log('no download link for:' + str(site) + ' ' + str(id))
            return False
        else:

            ## save to cloud
            if self.cloud_open_connection:
                cloud_download_link =  self.cloud_storage.store(id_subtitle, direct_download_link)
                print cloud_download_link
                Persistence.update_download_cloud_link(id_subtitle, cloud_download_link)
            else:
                Persistence.update_download_link(id_subtitle, direct_download_link)

        return True

    def crawler_results(self, parsed_file):
        title = parsed_file['title']
        season = parsed_file['season']
        episode = parsed_file['episode']
        year = parsed_file['year']
        hash_id = parsed_file['hash_id']

        for language in self.languages:
            language_iso_639_1 = language[SubtitleInfo.iso_639_1]

            ## iterating over sites
            for site in self.selected_sites:
                ## check if site has subtitles for the specified language
                if site.is_available_for_language(language):
                    session = self.sessions_searches[str(site)]

                    site_results_crawled = site.compute_results(title, season, episode, language,
                                                                hash_id, year, login=session)
                    ## check if there are results
                    if site_results_crawled:
                        Persistence.save_results(site_results_crawled,
                                                 hash_id,
                                                 language_iso_639_1,
                                                 site.id())

                        Analytics.search_update_date_added(hash_id)

        return


    @classmethod
    def download(cls, site, id, response_type=None):
        exists, direct_download_link = Persistence.get_download_link(site, id)


        if response_type == 'json':
            response_data = {
                'HTTPRESPONSE': 1,
                'url': direct_download_link
            }
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        elif response_type == 'httpresponse':
            return HttpResponseRedirect(direct_download_link)
        return direct_download_link


    @classmethod
    def download_id_subtitle(cls, id_subtitle, response_type=None):
        exists, direct_download_link = Persistence.get_download_link_id_subtitle(id_subtitle)

        if exists:
            ## download count statistics
            StatisticsController.download_id_subtitle(id_subtitle)

            if response_type == 'json':
                response_data = {
                    'HTTPRESPONSE': 1,
                    'url': direct_download_link
                }
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            elif response_type == 'httpresponse':
                return HttpResponseRedirect(direct_download_link)
            return direct_download_link
        return None

    @classmethod
    def get_pending_subtitles(cls, within_hours=1):
        return Persistence.get_pending_subtitles(within_hours, clean=True)


    @classmethod
    def get_pending_download_links(cls, limit, sites_array):
        return Persistence.get_pending_download_links(limit, sites_array)
