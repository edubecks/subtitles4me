__author__ = 'edubecks'
from django.db.models import F
from django.db import transaction

from subtitles.models import Subtitle, Statistics


from django.conf import settings
DEBUG = settings.DEBUG

class StatisticsController(object):

    @classmethod
    @transaction.autocommit
    def download_id_subtitle(cls, id_subtitle):
        subtitles = Statistics.objects.select_for_update().filter(id_subtitle=id_subtitle)
        for subtitle in subtitles:
            subtitle.download_count = F('download_count') + 1
            subtitle.save()
        return

    @classmethod
    def create(cls, id_subtitle):
        subtitle = Subtitle.objects.get(id_subtitle=id_subtitle)
        Statistics.objects.get_or_create(id_subtitle=subtitle)
        return

    @classmethod
    def last_subtitles(cls, number_of_subtitles=10):
        return
