#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Created on June 2, 2012
@author: edubecks
"""

import urllib
import shutil
import zipfile

import os

from subtitles.model.file_type import FileType


class FileDownloader():
    def __init__(self):
        pass

    TEMPORARY_NAME = 'temp'


    def download_file(self, file_type, link, directory, filename):
        if DEVELOPMENT: print 'Downloading: ', link
        if DEVELOPMENT: print 'writing to: ', filename
        if DEVELOPMENT: print 'in: ', directory

        compressedFilename = directory + FileDownloader.TEMPORARY_NAME
        filename = directory + filename

        downloadedFile = urllib.urlopen(link)
        # Open our local file for writing
        local_file = open(compressedFilename, "w")
        #Write to our local file
        local_file.write(downloadedFile.read())
        local_file.close()

        if file_type == FileType.SRT_FILE:
            os.rename(compressedFilename, filename)
        elif file_type == FileType.ZIP_FILE:
            if DEVELOPMENT: print 'ZIP: ', compressedFilename, ' ->', filename
            self.__handleZipFile(compressedFilename, filename)
        elif file_type == FileType.RAR_FILE:
            if DEVELOPMENT: print 'RAR: ', compressedFilename, ' ->', filename
            self.__handleRarFile(compressedFilename, filename)


    def __handleZipFile(self, compressedFilename, finalFilename):
        zipFile = zipfile.ZipFile(compressedFilename, 'r')
        for member in zipFile.namelist():
            filename = os.path.basename(member)
            # skip directories
            if not filename:
                continue

            # copy file (taken from zipfile's extract)
            source = zipFile.open(member)
            target = file(finalFilename, "w")
            shutil.copyfileobj(source, target)
            source.close()
            target.close()

            ## cleaning
            zipFile.close()
            os.remove(compressedFilename)
            return


    def __handleRarFile(self, compressedFilename, finalFilename):
        if DEVELOPMENT: print 'extracting: ' + compressedFilename
        rarFile = UnRAR2.RarFile(compressedFilename)
        for member in rarFile.infoiter():
            if member.isdir:
                continue

            rarFile.extract(condition=member.filename)
            os.rename(member.filename, finalFilename)

            ## cleaning
            rarFile.destruct()
            os.remove(compressedFilename)
            return

    def test(self):
        print DEVELOPMENT


def main():
    global DEVELOPMENT
    DEVELOPMENT = True

    fileDownloader = FileDownloader()
    fileDownloader.test()
    pass


if (__name__ == '__main__'):
    main()
