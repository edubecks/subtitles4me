# coding: utf-8
from datetime import datetime
from django.db import transaction
from django.db.models import F
from django.utils.timezone import utc

__author__ = 'edubecks'


from subtitles.models import AnalyticsModel, AnalyticsSearch

class Analytics(object):
    """
    Analytics for several features
    these will be used to improve the most used features
    """


    @classmethod
    @transaction.autocommit
    def __update(cls, source):
        analytic, created = AnalyticsModel.objects.get_or_create(source=source)
        analytic.counter = F('counter') + 1
        analytic.save()
        return

    @classmethod
    def search_single(cls):
        Analytics.__update(source='search_single')
        return

    @classmethod
    def search_multiple(cls):
        Analytics.__update(source='search_multiple')
        return

    @classmethod
    def source(cls, source):
        Analytics.__update(source=str(source))
        return

    @classmethod
    def site(cls, site):
        Analytics.__update(source='site_'+str(site))
        return

    @classmethod
    def language(cls, language):
        Analytics.__update(source='language_'+str(language))
        return


    @classmethod
    @transaction.autocommit
    def search_filename_result(cls, filename, hash_id):
        """
        update counter for each filename
        this is necessary to get the most downloaded results
        """
        analytic, created = AnalyticsSearch.objects.get_or_create(filename=filename.strip(), hash_id=hash_id)
        analytic.counter = F('counter') + 1
        analytic.save()
        return

    @classmethod
    def search_update_date_added(cls, hash_id):
        """
        update search results for every  item with the same hash_id
        this is necessary to show the last results
        """
        subtitles = AnalyticsSearch.objects.filter(hash_id=hash_id)
        for subtitle in subtitles:
            if not subtitle.has_results:
                subtitle.date_added = datetime.utcnow().replace(tzinfo=utc)
                subtitle.has_results = True
                subtitle.save()

        return

    @classmethod
    def create(cls, filename, hash_id):
        """
        adding results to analytics
        """
        AnalyticsSearch.objects.get_or_create(filename=filename, hash_id=hash_id)
        return

    @classmethod
    def get_last_subtitles(cls, num_results=50):
        return AnalyticsSearch.objects.filter(
            has_results=True).order_by('-date_added', 'counter').distinct('hash_id')[:num_results]





