import os
from subtitles.model.sites.open_subtitles.open_subtitles_utilities import OSDBServer


def search_subtitles( file_original_path, title, tvshow, year, season, episode, set_temp, rar, lang1, lang2, lang3, stack ): #standard input
    ok = False
    msg = ""
    hash_search = False
    subtitles_list = []

    if len(tvshow) > 0:                                            # TvShow
        OS_search_string = ("%s S%.2dE%.2d" % (tvshow,
                                               int(season),
                                               int(episode),)
            ).replace(" ","+")
    else:                                                          # Movie or not in Library
        if str(year) == "":                                          # Not in Library
            title, year = xbmc.getCleanMovieTitle( title )
        else:                                                        # Movie in Library
            year  = year
            title = title
        OS_search_string = title.replace(" ","+")

    if set_temp :
        hash_search = False
        file_size   = "000000000"
        SubHash     = "000000000000"
    else:
        file_size   = ""
        SubHash     = ""
        hash_search = False

    subtitles_list, msg = OSDBServer().searchsubtitles( OS_search_string, lang1, lang2, lang3, hash_search, SubHash, file_size  )

    return subtitles_list, "", msg #standard output



def download_subtitles (subtitles_list, pos, zip_subs, tmp_sub_dir, sub_folder, session_id): #standard input

    destination = os.path.join(tmp_sub_dir, "%s.srt" % subtitles_list[pos][ "ID" ])
    result = OSDBServer().download(subtitles_list[pos][ "ID" ], destination, session_id)
    if not result:
        import urllib
        urllib.urlretrieve(subtitles_list[pos][ "link" ],zip_subs)

    language = subtitles_list[pos][ "language_name" ]
    return not result,language, destination #standard output