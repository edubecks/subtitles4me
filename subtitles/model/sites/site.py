from pprint import pprint
from subtitles.model.log import log

__author__ = 'edubecks'

from unidecode import unidecode

from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.file_type import FileType


class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Site(object):
    __metaclass__ = Singleton


    def login_download(self, user=0):
        return None

    def login_search(self, user=0):
        return None

    def login(self, user=0, search=True, download=True):
        if search:
            self.session_search = self.login_search(user)
        if download:
            self.session_download = self.login_download(user)

    def file_type(self):
        return FileType.AUTOMATIC_FILE

    def available_languages(self):
        return SubtitleInfo.LANGUAGES.keys()

    def is_available_for_language(self, language):
        for each_language in self.available_languages():
            if language[SubtitleInfo.iso_639_1] == each_language['language']:
                return True
        return False

    def score_for_language(self, language):
        for each_language in self.available_languages():
            if language[SubtitleInfo.iso_639_1] == each_language['language']:
                return each_language['score']
        return 0


    def get_direct_download_link(self, result, login=None):
        response = self.download_subtitles(
            subtitles_list=[result],
            pos=0,
            zip_subs=None,
            tmp_sub_dir=None,
            sub_folder=None,
            session_id=login if login else self.login_download(),
        )
        return response

    def download_subtitles(self, subtitles_list, pos, zip_subs, tmp_sub_dir, sub_folder,
                           session_id): #standard input
        return

    def search_subtitles(self, file_original_path, title, tvshow, year, season, episode, set_temp, rar,
                         lang1, lang2, lang3, stack , session_id): #standard input
        return


    def __append_extra_info(self, results):
        for result_per_language in results:
            for result in result_per_language:
                if result:
                    ## todo optimizar la siguiente funcion para reducir el ancho de banda,
                    ## description = release
                    result['score'] = 0
                    result['release'] = unidecode(result['filename']).strip('\n\t\s\.\;')
                    result['site'] = self.__str__()
                    result['description'] = result['release']
                    result['extra'] = ''
        return results[0]

    def __sanitize(self, results):
        for result in results:
            for key,value in result.iteritems():
                if isinstance(value, basestring):
                    result[key] =  unidecode(value)

    def compute_results(self, title, season, episode, language, hash_id, year, login=None):
        """
        This function computes the score for a given tv series, the result is a
        value between 0 and 1, 1 being a perfect match
        """
        results = self.search_subtitles(
            file_original_path='',
            title=title,
            tvshow=title if season else '',
            year=year,
            season=season,
            episode=episode,
            set_temp=True,
            rar=None,
            lang1=language[SubtitleInfo.iso_639_1],
            lang2=None,
            lang3=None,
            stack=None,
            session_id = login if login else self.login_search(),
        )
        results = self.__append_extra_info(results)
        self.__sanitize(results)
        return results

    def id(self):
        return self.NAME

    def __str__(self):
        return self.NAME