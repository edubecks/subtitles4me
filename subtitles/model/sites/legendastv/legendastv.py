__author__ = 'edubecks'

from subtitles.model.sites.site import Site
from subtitles.model.sites.legendastv.legendastv_service import *
from subtitles.model.utilities import EXTRA
from subtitles.model.subtitle_info import SubtitleInfo

class LegendasTV(Site):
    NAME = 'Legendas TV'

    def file_type(self):
        from subtitles.model.file_type import FileType
        return FileType.SRT_FILE


    def __init__(self):
        super(LegendasTV, self).__init__()
        self.search_subtitles = search_subtitles
        self.download_subtitles = download_subtitles
        self.login_download = LegendasLogin
        self.login_search = LegendasLogin

    def available_languages(self):
        return [
            {
                'language': SubtitleInfo.PORTUGUESE_BR[SubtitleInfo.iso_639_1],
                'score': 1
            },
            {
                'language': SubtitleInfo.PORTUGUESE[SubtitleInfo.iso_639_1],
                'score': 1
            },
        ]


    def compute_results(self, title, season, episode, language, hash_id, year, login=None):
        results = super(LegendasTV, self).compute_results(title, season, episode, language, hash_id,
                                                          year, login)
        return results