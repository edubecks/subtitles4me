# coding: utf-8
__author__ = 'edubecks'

__author__ = 'edubecks'
from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.sites.site import Site
from subtitles.model.sites.subscene.subscene_service import *
from subtitles.model.utilities import EXTRA

class Subscene(Site):

    NAME = 'Subscene'

    def __init__(self):
        super(Subscene, self).__init__()
        self.search_subtitles = search_subtitles
        self.download_subtitles = download_subtitles


    def available_languages(self):
        return [SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1]]

    def compute_results(self, title, season, episode, language, hash_id, year, login=None):
        results = super(Subscene, self).compute_results(title, season, episode, language, hash_id, year, login)
        for result in results:
            result[EXTRA] = {'server': result['server'], 'language_name': result['language_name']}
        return results
