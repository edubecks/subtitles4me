__author__ = 'edubecks'

from collections import OrderedDict
from subtitles.model.sites.podnapisi.podnapisi import Podnapisi
from subtitles.model.sites.subdivx.subdivx import Subdivx
from subtitles.model.sites.legendastv.legendastv import LegendasTV
from subtitles.model.sites.legendaszone.legendaszone import LegendasZone


podnapisi_site = Podnapisi()
subdivx_site = Subdivx()
legendastv_site = LegendasTV()
legendaszone_site = LegendasZone()

REGISTERED_SITES = OrderedDict([
    (Podnapisi.NAME, podnapisi_site),
    (Subdivx.NAME, subdivx_site),
    (LegendasTV.NAME, legendastv_site),
    (LegendasZone.NAME, legendaszone_site)
])

