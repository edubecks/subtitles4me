__author__ = 'edubecks'

from subtitles.model.sites.site import Site
from subtitles.model.sites.legendaszone.legendaszone_service import *
from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.file_type import FileType


class LegendasZone(Site):
    NAME = 'Legendas Zone'

    def file_type(self):
        return FileType.SRT_FILE


    def __init__(self):
        super(LegendasZone, self).__init__()
        self.search_subtitles = search_subtitles
        self.download_subtitles = download_subtitles
        self.login_download = legendaszone_login
        self.login_search = legendaszone_login

    def available_languages(self):
        return [
            {
                'language': SubtitleInfo.PORTUGUESE_BR[SubtitleInfo.iso_639_1],
                'score': 1
            },
            # {
            #     'language': SubtitleInfo.PORTUGUESE[SubtitleInfo.iso_639_1],
            #     'score': 1
            # },
        ]