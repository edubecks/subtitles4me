# coding: utf-8
__author__ = 'edubecks'

# -*- coding: utf-8 -*-

# Service Legendas-Zone.org version 0.2.1
# Code based on Undertext service and the download function encode fix from legendastv service
# Coded by HiGhLaNdR@OLDSCHOOL
# Help by VaRaTRoN
# Bugs & Features to highlander@teknorage.com
# http://www.teknorage.com
# License: GPL v2
#
# NEW on Service Legendas-Zone.org v0.2.1:
# Service working again, developers change the page way too much!
# Fixed download bug when XBMC is set to Portuguese language
# Removed IMDB search since they are always changing code!
# Some code cleanup
#
# NEW on Service Legendas-Zone.org v0.2.0:
# Fixed bug on openelec based XBMC prevent the script to work
# Removed some XBMC messages from the script who were annoying!
# Some code cleanup
#
# NEW on Service Legendas-Zone.org v0.1.9:
# Added all site languages (English, Portuguese, Portuguese Brazilian and Spanish)
# Changed the way it would handle several patterns for much better finding (site not well formed...)
# Messages now in xbmc choosen language.
# Added new logo.
# Fixed download.
# Code re-arrange...
#
# NEW on Service Legendas-Zone.org v0.1.8:
# Added uuid for better file handling, no more hangups.
#
# NEW on Service Legendas-Zone.org v0.1.7:
# Changed 2 patterns that were crashing the plugin, now it works correctly.
# Better builtin notifications for better information.
#
# NEW on Service Legendas-Zone.org v0.1.6:
# Better search results with 3 types of searching. Single title, multi titles and IMDB search.
# Added builtin notifications for better information.
#
# Initial Release of Service Legendas-Zone.org - v0.1.5:
# TODO: re-arrange code :)
#
# Legendas-Zone.org subtitles, based on a mod of Undertext subtitles
import os, sys, re,string, time, urllib, urllib2, cookielib, shutil, fnmatch, uuid
from subtitles.model.subtitle_info import languageTranslate
from subtitles.model.log import log
from subtitles.model.config import LEGENDAS_ZONE
import requests


main_url = "http://www.legendas-zone.org/"
debug_pretext = "Legendas-Zone"
subext = ['srt', 'aas', 'ssa', 'sub', 'smi']
sub_ext = ['srt', 'aas', 'ssa', 'sub', 'smi']
packext = ['rar', 'zip']
DEFAULT_EXTENSION = '.rar'

#====================================================================================================================
# Regular expression patterns
#====================================================================================================================

"""
"""
subtitle_pattern = "<b><a\shref=\"legendas.php\?modo=detalhes&amp;(.+?)\".+?[\r\n\t]+?.+?[\r\n\t]+?.+?onmouseover=\"Tip\(\'<table><tr><td><b>(.+?)</b></td></tr></table>.+?<b>Hits:</b>\s(.+?)\s<br>.+?<b>CDs:</b>\s(.+?)<br>.+?Uploader:</b>\s(.+?)</td>"
# group(1) = ID, group(2) = Name, group(3) = Hits, group(4) = Files, group(5) = Uploader
multiple_results_pattern = "<td\salign=\"left\".+?<b><a\shref=\"legendas.php\?imdb=(.+?)\"\stitle=\"(.+?)\">"
# group(1) = IMDB
imdb_pattern = "<td class=\"result_text\"> <a\shref=\"\/title\/tt(.+?)\/\?"
# group(1) = IMDB
#====================================================================================================================
# Functions
#====================================================================================================================
def __from_utf8(text):
    if isinstance(text, str):
        return text.decode('utf-8')
    else:
        return text



def getallsubs(searchstring, languageshort, languagelong, file_original_path, subtitles_list,
               searchstring_notclean):

    page = 0
    if languageshort == "pb":
        languageshort = "br"
    if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
        url = main_url + "legendas.php?l=" + languageshort + "&page=" + \
              str(page) + "&s=" + urllib.quote_plus(searchstring)
    else:
        url = main_url + "index.php"

    content = urllib2.urlopen(url)
    #log( __name__ ,"%s Content: '%s'" % (debug_pretext, content))
    content = content.read().decode('latin1')
    #log( __name__ ,"%s Contentread: '%s'" % (debug_pretext, content.decode('latin1')))

    ## check if multiple pages in results
    if re.search(multiple_results_pattern, content,
                 re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE) is None:
        # log(__name__, "%s LangSingleSUBS: '%s'" % (debug_pretext, languageshort))
        # log(__name__, "%s Getting '%s' subs ..." % (debug_pretext, "Single Title"))
        while re.search(subtitle_pattern, content,
                        re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE) and page < 3:
            for matches in re.finditer(subtitle_pattern, content,
                                       re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE):
                hits = matches.group(3)
                downloads = int(matches.group(3)) / 5
                if downloads > 10:
                    downloads = 10
                id = matches.group(1)
                filename = string.strip(matches.group(2))
                desc = string.strip(matches.group(2))
                no_files = matches.group(4)
                #Remove new lines on the commentaries
                filename = re.sub('\n', ' ', filename)
                desc = re.sub('\n', ' ', desc)
                desc = re.sub('&quot;', '"', desc)
                #Remove HTML tags on the commentaries
                filename = re.sub(r'<[^<]+?>', '', filename)
                desc = re.sub(r'<[^<]+?>|[~]', ' ', desc)
                #Find filename on the comentaries to show sync label using filename or dirname (making it global for further usage)
                if languageshort == "br":
                    languageshort = "pb"
                subtitles_list.append(
                    {
                        'rating': str(downloads),
                        'no_files': no_files,
                        'id': id,
                        'filename': filename,
                        'desc': desc,
                        'hits': hits,
                        'language_flag': 'flags/' + languageshort + '.gif',
                        'language_name': languagelong
                    }
                )
            page += 1
            if languageshort == "br":
                languageshort = "pb"
                #			url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(page) + "&s=" + urllib.quote_plus(searchstring)
            if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
                url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(
                    page) + "&s=" + urllib.quote_plus(searchstring)
            else:
                url = main_url + "index.php"
            content = urllib2.urlopen(url)
            content = content.read().decode('latin1')
            #For DEBUG only uncomment next line
            #log( __name__ ,"%s Getting '%s' list part xxx..." % (debug_pretext, content))
    else:
        page = 0
        if languageshort == "pb":
            languageshort = "br"
        if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
            url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(
                page) + "&s=" + urllib.quote_plus(searchstring)
        else:
            url = main_url + "index.php"
            #url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(page) + "&s=" + urllib.quote_plus(searchstring)
        content = urllib2.urlopen(url)
        content = content.read().decode('latin1')
        maxsubs = re.findall(multiple_results_pattern, content,
                             re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE)
        #maxsubs = len(maxsubs)
        if maxsubs != "":
            # log(__name__, "%s LangMULTISUBS: '%s'" % (debug_pretext, languageshort))
            #log( __name__ ,"%s Getting '%s' subs ..." % (debug_pretext, "Less Then 10 Titles"))
            while re.search(multiple_results_pattern, content,
                            re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE) and page < 1:
                for resmatches in re.finditer(multiple_results_pattern, content,
                                              re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE):
                    imdb = resmatches.group(1)
                    imdb_title = resmatches.group(2).lower()
                    print searchstring, imdb_title
                    if searchstring in imdb_title:
                        page1 = 0
                        if languageshort == "pb":
                            languageshort = "br"
                        if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
                            temp_url = (main_url + "legendas.php?l=" + languageshort + "&imdb=" + imdb +
                                       "&page=" + str(page1))
                            content1 = urllib2.urlopen(temp_url)
                        else:
                            content1 = main_url + "index.php"
                            #content1 = opener.open(main_url + "legendas.php?l=" + languageshort + "&imdb=" + imdb + "&page=" + str(page1))
                        content1 = content1.read()
                        content1 = content1.decode('latin1')
                        while re.search(subtitle_pattern, content1,
                                        re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE) and page1 == 0:
                            for matches in re.finditer(subtitle_pattern, content1,
                                                       re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE | re.VERBOSE):
                                #log( __name__ ,"%s PAGE? '%s'" % (debug_pretext, page1))
                                hits = matches.group(3)
                                downloads = int(matches.group(3)) / 5
                                if downloads > 10:
                                    downloads = 10
                                id = matches.group(1)
                                filename = string.strip(matches.group(2))
                                desc = string.strip(matches.group(2))
                                #desc = filename + " - uploader: " + desc
                                no_files = matches.group(4)
                                uploader = matches.group(5)
                                #log( __name__ ,"%s filename '%s'" % (debug_pretext, filename))
                                filename_check = string.split(filename, ' ')
                                #log( __name__ ,"%s filename '%s'" % (debug_pretext, filename_check))
                                #Remove new lines on the commentaries
                                filename = re.sub('\n', ' ', filename)
                                desc = re.sub('\n', ' ', desc)
                                desc = re.sub('&quot;', '"', desc)
                                #Remove HTML tags on the commentaries
                                filename = re.sub(r'<[^<]+?>', '', filename)
                                desc = re.sub(r'<[^<]+?>|[~]', ' ', desc)
                                #Find filename on the comentaries to show sync label using filename or dirname (making it global for further usage)
                                filename = filename + "  " + hits + "Hits" + " - " + desc + " - uploader: " + uploader
                                if languageshort == "br":
                                    languageshort = "pb"
                                subtitles_list.append(
                                    {'rating': str(downloads), 'no_files': no_files, 'id': id,
                                     'filename': filename, 'desc': desc, 'hits': hits,
                                     'language_flag': 'flags/' + languageshort + '.gif',
                                     'language_name': languagelong})
                            page1 += 1
                            if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
                                temp_url = (main_url + "legendas.php?l=" + languageshort + "&imdb=" + imdb +
                                           "&page=" + str(page1))
                                content1 = urllib2.urlopen(temp_url)
                            else:
                                content1 = main_url + "index.php"
                                content1 = urllib2.urlopen(content1)
                                #content1 = opener.open(main_url + "legendas.php?l=" + languageshort + "&imdb=" + imdb + "&page=" + str(page1))
                            content1 = content1.read().decode('latin1')
                page += 1
                if languageshort == "pb":
                    languageshort = "br"
                    #url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(page) + "&s=" + urllib.quote_plus(searchstring)
                if languageshort == "pt" or languageshort == "br" or languageshort == "en" or languageshort == "es":
                    url = main_url + "legendas.php?l=" + languageshort + "&page=" + str(
                        page) + "&s=" + urllib.quote_plus(searchstring)
                else:
                    url = main_url + "index.php"
                content = urllib2.urlopen(url)
                content = content.read().decode('latin1')



def search_subtitles( file_original_path, title, tvshow, year, season, episode, set_temp, rar,
                      lang1, lang2, lang3, stack, session_id): #standard input
    subtitles_list = []
    msg = ""
    searchstring_notclean = ""
    searchstring = ""
    global israr
    israr = os.path.abspath(file_original_path)
    israr = os.path.split(israr)
    israr = israr[0].split(os.sep)
    israr = string.split(israr[-1], '.')
    israr = string.lower(israr[-1])

    if len(tvshow) == 0:
        if 'rar' in israr and searchstring is not None:
            if 'cd1' in string.lower(title) or 'cd2' in string.lower(title) or 'cd3' in string.lower(
                    title):
                dirsearch = os.path.abspath(file_original_path)
                dirsearch = os.path.split(dirsearch)
                dirsearch = dirsearch[0].split(os.sep)
                if len(dirsearch) > 1:
                    searchstring_notclean = dirsearch[-3]
                    searchstring = searchstring[0]
                else:
                    searchstring = title
            else:
                searchstring = title
        elif 'cd1' in string.lower(title) or 'cd2' in string.lower(title) or 'cd3' in string.lower(
                title):
            dirsearch = os.path.abspath(file_original_path)
            dirsearch = os.path.split(dirsearch)
            dirsearch = dirsearch[0].split(os.sep)
            if len(dirsearch) > 1:
                searchstring_notclean = dirsearch[-2]
                searchstring = searchstring[0]
            else:
                #We are at the root of the drive!!! so there's no dir to lookup only file#
                title = os.path.split(file_original_path)
                searchstring = title[-1]
        else:
            if title == "":
                title = os.path.split(file_original_path)
                searchstring = title[-1]
            else:
                searchstring = title

    if len(tvshow) > 0:
        searchstring = "%s S%#02dE%#02d" % (tvshow, int(season), int(episode))
    # log(__name__, "%s Search string = %s" % (debug_pretext, searchstring))

    hasLang = (languageTranslate(lang1, 0, 2) + " " +
               languageTranslate(lang2, 0, 2) + " " +
               languageTranslate(lang3, 0, 2)
    )

    if re.search('pt', hasLang) or re.search('en', hasLang) or re.search('es', hasLang) or re.search(
            'pb', hasLang):
        getallsubs(searchstring, languageTranslate(lang1, 0, 2), lang1, file_original_path,
                   subtitles_list, searchstring_notclean)
        getallsubs(searchstring, languageTranslate(lang2, 0, 2), lang2, file_original_path,
                   subtitles_list, searchstring_notclean)
        getallsubs(searchstring, languageTranslate(lang3, 0, 2), lang3, file_original_path,
                   subtitles_list, searchstring_notclean)
    else:
        msg = "Won't work, LegendasZone.com is only for PT, PTBR, ES or EN subtitles."

    return subtitles_list, "", msg #standard output


def download_subtitles(subtitles_list, pos, zip_subs, tmp_sub_dir, sub_folder,
                       session_id): #standard input

    id = subtitles_list[pos]

    #This is where you are logged in and download
    download_url = main_url + 'downloadsub.php'

    req_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13',
        'Referer': main_url,
        'Keep-Alive': '300',
        'Connection': 'keep-alive'}

    download_data = urllib.urlencode({'sid': id, 'submit': '+', 'action': 'Download'})
    request1 = urllib2.Request(download_url, download_data, req_headers)
    content = urllib2.urlopen(request1)

    filename = content.info()['Content-Disposition'].strip()
    # print(content.info())
    # print(filename)
    extension_index = filename.rfind('.')
    extension =  filename[extension_index:-1]


    return {
        'extension': extension if extension else DEFAULT_EXTENSION,
        'file':content,
        'filename':filename
    }


def legendaszone_login(user=0, cj=0):
    if cj:
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        urllib2.install_opener(opener)
    else:

        ## getting user and password
        user %= len(LEGENDAS_ZONE)
        username, password = LEGENDAS_ZONE[user]['user'], LEGENDAS_ZONE[user]['password']

        cj = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        urllib2.install_opener(opener)

        req_headers = {
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13',
            'Referer': main_url,
            'Keep-Alive': '300',
            'Connection': 'keep-alive'}
        login_data = urllib.urlencode({'username': username, 'password': password})
        request = urllib2.Request(main_url + 'fazendologin.php', login_data, headers=req_headers)
        response = urllib2.urlopen(request).read()

        # opener.addheaders.append(('User-agent', 'Mozilla/4.0'))
        # r = opener.open(main_url + 'fazendologin.php', login_data)

    return cj

