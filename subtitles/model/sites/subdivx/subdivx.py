__author__ = 'edubecks'
from subtitles.model.subtitle_info import SubtitleInfo
from subtitles.model.sites.site import Site
from subtitles.model.sites.subdivx.subdivx_service import *


class Subdivx(Site):
    NAME = 'Subdivx'

    def __init__(self):
        super(Subdivx, self).__init__()
        self.search_subtitles = search_subtitles
        self.download_subtitles = download_subtitles

    def login_download(self, user=0):
        return 'user_' + str(user) + '_OK'

    def available_languages(self):
        return [
            {
                'language': SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1],
                'score': 1
            },
        ]

    def compute_results(self, title, season, episode, language, hash_id, year, login=None):
        results = super(Subdivx, self).compute_results(title, season, episode, language, hash_id, year,
                                                       login)
        for result in results:
            result['extra'] = {'server': result['server']}
        return results
