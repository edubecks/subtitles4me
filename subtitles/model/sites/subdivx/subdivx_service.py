
# -*- coding: utf-8 -*-

# Subdivx.com subtitles, based on a mod of Undertext subtitles
import string
import urllib
import urllib2

import os
import re
from unidecode import unidecode

from subtitles.model.log import log

import multiprocessing
from multiprocessing import Queue, Process
from Queue import Empty as QueueEmpty




main_url = "http://www.subdivx.com/"
debug_pretext = "subdivx"

#====================================================================================================================
# Regular expression patterns
#====================================================================================================================


#Subtitle pattern example:
"""
<div id="menu_titulo_buscador"><a class="titulo_menu_izq" href="http://www.subdivx.com/X6XMjEzMzIyX-iron-man-2-2010.html">Iron Man 2 (2010)</a></div>
<img src="img/calif5.gif" class="detalle_calif">
</div><div id="buscador_detalle">
<div id="buscador_detalle_sub">Para la versión Iron.Man.2.2010.480p.BRRip.XviD.AC3-EVO, sacados de acá. ¡Disfruten!</div><div id="buscador_detalle_sub_datos"><b>Downloads:</b> 4673 <b>Cds:</b> 1 <b>Comentarios:</b> <a rel="nofollow" href="popcoment.php?idsub=MjEzMzIy" onclick="return hs.htmlExpand(this, { objectType: 'iframe' } )">14</a> <b>Formato:</b> SubRip <b>Subido por:</b> <a class="link1" href="http://www.subdivx.com/X9X303157">TrueSword</a> <img src="http://www.subdivx.com/pais/2.gif" width="16" height="12"> <b>el</b> 06/09/2010  <a rel="nofollow" target="new" href="http://www.subdivx.com/bajar.php?id=213322&u=6"><img src="bajar_sub.gif" border="0"></a></div></div>
<div id="menu_detalle_buscador">
"""

__subtitle_pattern =  "<a\sclass=\"titulo_menu_izq\"\shref=\"(.*?)\".*?<div\sid=\"buscador_detalle_sub\">(.+?)<.*?/div><div\sid=\"buscador_detalle_sub_datos\"><b>Downloads:</b>(.+?)<b>Cds:</b>(.+?)<b>Comentarios:</b>\s.+?\s<b>Formato:</b>.+?<b>Subido\spor:</b>\s<a\sclass=\"link1\".*?"
# group(1) = user comments, may content filename, group(2)= downloads used for ratings, group(3) = #files, group(4) = id, group(5) = server

__subtitle_pattern_id_server="href=\"http://www.subdivx.com/bajar.php\?id=(.+?)&u=(.+?)\">Bajar"

#====================================================================================================================
# Functions
#====================================================================================================================


def __get_id_and_server(url):
    content = geturl(url)
    for matches in re.finditer(__subtitle_pattern_id_server, content, re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE):
        ## return id, server
        return matches.group(1), matches.group(2)
    return None

def __process_matches(matches):
    url_for_subtitle = matches['url']
    if url_for_subtitle:
        subdivx_id, server = __get_id_and_server(url_for_subtitle)
        filename = string.strip(matches['filename'])
        #Remove new lines on the commentaries
        filename = re.sub('\n',' ',filename)
        #Remove HTML tags on the commentaries
        filename = re.sub(r'<[^<]+?>','', filename)
        #Find filename on the comentaries to show sync label
        subtitle = {
                'filename': filename,
                'id' : subdivx_id,
                'server' : server,
            }
        return subtitle

def __worker(queue_matches, queue_subtitles):
    """Put the line into the queue out"""
    while True:
        try:
            matches = queue_matches.get(timeout = 1) # Timeout after 1 second
            print matches
            subtitle = __process_matches(matches)
            if subtitle:
                queue_subtitles.put(subtitle)
        except QueueEmpty:
            return # Exit when all work is done
        except:
            raise # Raise all other errors


def __join_results(queue_subtitles, subtitles_list):
    while True:
        try:
            new_subtitle = queue_subtitles.get(timeout=1)
            subtitles_list.append(new_subtitle)
        except QueueEmpty:
            return
        except:
            raise


def __getallsubs(searchstring, languageshort, languagelong, file_original_path, subtitles_list, max_results=20):
    page = 1

    def url_for_page(page):
        return main_url + "index.php?accion=5&masdesc=&oxfecha=2&pg=" + str(page) + "&buscar=" + urllib.quote_plus(searchstring)

    url = url_for_page(page)
    content = geturl(url)

    num_threads = 4

    ## wait for processes
    jobs = []

    ## processing queue input
    queue_matches = Queue()
    ## number to control the max results
    subtitles_crawled = 0

    ## processing queue output
    queue_subtitles = Queue()

    while re.search(__subtitle_pattern, content, re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE):

        ## enqueuing results
        for matches in re.finditer(__subtitle_pattern, content, re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE):
            queue_matches.put({
                'url': matches.group(1),
                'filename': unidecode(matches.group(2)),
            })
            subtitles_crawled += 1

        if subtitles_crawled > max_results:
            break

        ## next results page
        page += 1
        url = url_for_page(page)
        content = geturl(url)

    # Start consumers
    for i in range(num_threads):
        p = Process(target = __worker, args = (queue_matches, queue_subtitles, ))
        jobs.append(p)
        p.start()

    # Final process to handle the queue out
    for job in jobs :
        job.join()
    __join_results(queue_subtitles, subtitles_list)
    return





def geturl(url):
    class MyOpener(urllib.FancyURLopener):
        version = ''
    my_urlopener = MyOpener()
    try:
        response = my_urlopener.open(url)
        content    = response.read()
    except:
        log( '%s Failed to get url:%s' % (debug_pretext, url), __name__ )
        content    = None
    return content


def search_subtitles( file_original_path, title, tvshow, year, season, episode, set_temp, rar, lang1, lang2, lang3, stack, session_id): #standard input
    subtitles_list = []
    msg = ""
    if not len(tvshow):
        searchstring = title
    else:
        searchstring = '%s S%#02dE%#02d' % (tvshow, int(season), int(episode))
    if len(title)<= 3:
        searchstring = title + ' ' + year

    __getallsubs(searchstring, 'es', 'Spanish', file_original_path, subtitles_list)


    return subtitles_list, "", msg #standard output


def download_subtitles (subtitles_list, pos, zip_subs, tmp_sub_dir, sub_folder, session_id): #standard input
    id = subtitles_list[pos]['id']
    server = subtitles_list[pos]['extra']['server']
    url = main_url + 'bajar.php?id=' + id + '&u=' + server
    try:
        direct_download_link = urllib2.urlopen(url).geturl()
        return direct_download_link
    except (urllib2.HTTPError, urllib2.URLError):
        return 'broken_'+url

