from subtitles.model.subtitle_info import SubtitleInfo

__author__ = 'edubecks'

from subtitles.model.sites.site import Site
from subtitles.model.sites.podnapisi.podnapisi_service import *
from subtitles.model.sites.podnapisi.podnapisi_utilities import OSDBServer

class Podnapisi(Site):

    NAME = 'Podnapisi'

    def file_type(self):
        from subtitles.model.file_type import FileType
        return FileType.ZIP_FILE

    def __init__(self):
        super(Podnapisi, self).__init__()
        self.search_subtitles = search_subtitles
        self.download_subtitles = download_subtitles
        self.login_download = OSDBServer.login
        return


    def available_languages(self):
        return [
            {
                'language': SubtitleInfo.SPANISH[SubtitleInfo.iso_639_1],
                'score': 0.9
            },
            {
                'language': SubtitleInfo.ENGLISH_US[SubtitleInfo.iso_639_1],
                'score': 1
            },
        ]

    def compute_results(self, title, season, episode, language, hash_id, year, login=None):
        results = super(Podnapisi, self).compute_results(title, season, episode, language, hash_id, year, login)
        return results