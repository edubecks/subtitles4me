# -*- coding: utf-8 -*- 

from subtitles.model.subtitle_info import languageTranslate
from subtitles.model.sites.podnapisi.podnapisi_utilities import OSDBServer

def search_subtitles( file_original_path, title, tvshow, year, season, episode, set_temp, rar, lang1, lang2, lang3, stack, session_id): #standard input
    osdb_server = OSDBServer()
    osdb_server.create()
    subtitles_list = []
    language1 = languageTranslate(lang1,0,1)
    language2 = None
    language3 = None
    if set_temp :
        hash_search = False
        file_size   = "000000000"
        SubHash     = "000000000000"
    else:
        file_size   = ""
        SubHash     = ""
        hash_search = False

#    if hash_search :
#        subtitles_list, session_id = osdb_server.searchsubtitles_pod( SubHash ,language1, language2, language3, stack)
#    if not subtitles_list:
    subtitles_list = osdb_server.searchsubtitlesbyname_pod( title, tvshow, season, episode, language1, language2, language3, year, stack )
    return subtitles_list, "", "" #standard output

def download_subtitles (subtitles_list, pos, zip_subs, tmp_sub_dir, sub_folder, session_id): #standard input
    osdb_server = OSDBServer()
    url = osdb_server.download(session_id, subtitles_list[pos][ "id" ])
    return url


