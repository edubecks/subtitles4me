# -*- coding: utf-8 -*- 

import xmlrpclib
from xml.dom import minidom
import urllib
import re
import requests

from subtitles.model.log import log
from subtitles.model.config import PODNAPISI
from subtitles.model.config import SCRIPTNAME, VERSION
from subtitles.model.subtitle_info import languageTranslate
from xml.parsers.expat import ExpatError


# Python 2.6 +
from hashlib import md5 as md5
from hashlib import sha256


__scriptname__ = SCRIPTNAME
__version__ = VERSION

USER_AGENT = "%s_v%s" % (__scriptname__.replace(" ","_"),__version__ )

def compare_columns(b,a):
    return cmp( b["language_name"], a["language_name"] )  or cmp( a["sync"], b["sync"] )

class OSDBServer:
    def create(self):
        self.subtitles_hash_list = []
        self.subtitles_list = []
        self.subtitles_name_list = []

    def mergesubtitles( self, stack ):
        if len ( self.subtitles_hash_list ) > 0:
            for item in self.subtitles_hash_list:
                if item["format"].find( "srt" ) == 0 or item["format"].find( "sub" ) == 0:
                    self.subtitles_list.append( item )

        if len ( self.subtitles_name_list ) > 0:
            for item in self.subtitles_name_list:
                if item["format"].find( "srt" ) == 0 or item["format"].find( "sub" ) == 0:
                    self.subtitles_list.append( item )

        if len ( self.subtitles_list ) > 0:
            self.subtitles_list = sorted(self.subtitles_list, compare_columns)

    def searchsubtitles_pod( self, movie_hash, lang1,lang2,lang3, stack):
    #    movie_hash = "e1b45885346cfa0b" # Matrix Hash, Debug only
        podserver = xmlrpclib.Server('http://ssp.podnapisi.net:8000')
        pod_session = ""
        hash_pod =[str(movie_hash)]
        lang = [lang1]
        try:
            init = podserver.initiate(USER_AGENT)
            hash = md5()
            hash.update(PASSWORD)
            password256 = sha256(str(hash.hexdigest()) + str(init['nonce'])).hexdigest()
            if str(init['status']) == "200":
                pod_session = init['session']
                podserver.authenticate(pod_session, USER, password256)
                podserver.setFilters(pod_session, True, lang , False)
                search = podserver.search(pod_session , hash_pod)
                if str(search['status']) == "200" and len(search['results']) > 0 :
                    search_item = search["results"][movie_hash]
                    for item in search_item["subtitles"]:
                        if item["lang"]:
                            flag_image = "flags/%s.gif" % (item["lang"],)
                        else:
                            flag_image = "-.gif"
                        link = str(item["id"])
                        if item['release'] == "":
                            episode = search_item["tvEpisode"]
                            if str(episode) == "0":
                                name = "%s (%s)" % (str(search_item["movieTitle"]),str(search_item["movieYear"]),)
                            else:
                                name = "%s S(%s)E(%s)" % (str(search_item["movieTitle"]),str(search_item["tvSeason"]), str(episode), )
                        else:
                            name = item['release']
                        if item["inexact"]:
                            sync1 = False
                        else:
                            sync1 = True

                        self.subtitles_hash_list.append({'filename'      : name,
                                                         'link'          : link,
                                                         "language_name" : languageTranslate((item["lang"]),2,0),
                                                         "language_flag" : flag_image,
                                                         "language_id"   : item["lang"],
                                                         "id"            : item["id"],
                                                         "sync"          : sync1,
                                                         "format"        : "srt",
                                                         "rating"        : str(int(item['rating'])*2),
                                                         "hearing_imp"   : "n" in item['flags']
                        })
                self.mergesubtitles(stack)
            return self.subtitles_list,pod_session
        except :
            return self.subtitles_list,pod_session

    def searchsubtitlesbyname_pod( self, name, tvshow, season, episode, lang1, lang2, lang3, year, stack ):
        if len(tvshow) > 1:
            name = tvshow
        search_url_base = "http://www.podnapisi.net/ppodnapisi/search?tbsl=1&sK=%s&sJ=%s&sY=%s&sTS=%s&sTE=%s&sXML=1&lang=0" % (name.replace(" ","+"), "%s", str(year), str(season), str(episode))
        search_url = search_url_base % str(lang1)
        try:
            subtitles = self.fetch(search_url)
            if subtitles:
                url_base = "http://www.podnapisi.net/ppodnapisi/download/i/"
                for subtitle in subtitles:
                    subtitle_id = 0
                    rating      = 0
                    filename    = ""
                    movie       = ""
                    lang_name   = ""
                    lang_id     = ""
                    flag_image  = ""
                    link        = ""
                    format      = "srt"
                    hearing_imp = False
                    if subtitle.getElementsByTagName("title")[0].firstChild:
                        movie = subtitle.getElementsByTagName("title")[0].firstChild.data
                    if subtitle.getElementsByTagName("release")[0].firstChild:
                        filename = subtitle.getElementsByTagName("release")[0].firstChild.data
                        if len(filename) < 2 :
                            filename = "%s (%s).srt" % (movie,year,)
                    else:
                        filename = "%s (%s).srt" % (movie,year,)
                    if subtitle.getElementsByTagName("id")[0].firstChild:
                        subtitle_id = subtitle.getElementsByTagName("id")[0].firstChild.data
                    self.subtitles_name_list.append({'filename':filename,
                                                     'language' : lang1,
                                                     'movie'         : movie,
                                                     'id': subtitle_id,
                                                     'format': format,
                    })
                self.mergesubtitles(stack)
            return self.subtitles_list
        except :
            return self.subtitles_list

    def download(self,session_id,  id):
        podserver, pod_session = session_id if session_id else OSDBServer.login()
        try:
            download = podserver.download(pod_session , [id])
            if str(download['status']) == "200":
                if len(download['names']) > 0 :
                    if str(download["names"][0]['id']) == str(id):
                        return "http://www.podnapisi.net/static/podnapisi/%s" % download["names"][0]['filename']
        except:
            return None
        return self.alternative_download(id)




    @classmethod
    def login(cls, user=0):
        podserver = xmlrpclib.Server('http://ssp.podnapisi.net:8000')
        init = podserver.initiate(USER_AGENT)

        ## select user and password
        user = user % len(PODNAPISI)
        USER, PASSWORD = PODNAPISI[user]['user'], PODNAPISI[user]['password']

        hash = md5()
        hash.update(PASSWORD)
        password256 = sha256(str(hash.hexdigest()) + str(init['nonce'])).hexdigest()
        if str(init['status']) == "200":
            pod_session = init['session']
            auth = podserver.authenticate(pod_session, USER, password256)
            if auth['status'] == 300:
                log( "Authenticate [%s]" % "InvalidCredentials", __name__ )
            elif auth['status'] == 301:
                log( "Authenticate [%s]" % "NoAuthorisation", __name__ )
            elif auth['status'] == 302:
                log( "Authenticate [%s]" % "InvalidSession", __name__ )
            return podserver, pod_session

    def fetch(self,url):
        socket = urllib.urlopen( url )
        result = socket.read()
        socket.close()
        xmldoc = minidom.parseString(result)
        return xmldoc.getElementsByTagName("subtitle")


    def alternative_download(self, id):
        url = 'http://www.podnapisi.net/en/ppodnapisi/download/i/'+str(id)
        user_agent = {'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 '
                                    '(KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17'}
        html = requests.put(url, headers=user_agent).text
        LINK_REGEX = 'href=\"(.*?)\".*>Download subtitle'
        regex= re.compile(LINK_REGEX)
        its_match = regex.search(html)
        if its_match:
            download_link = 'http://www.podnapisi.net' + its_match.group(1)
            if  not download_link.startswith('http://www.podnapisi.net/static/'):
                return None
            return download_link

