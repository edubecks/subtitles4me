#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Created on July 22, 2012
@author: edubecks
"""

import optparse

GENERAL = 'general'
TV_SERIES_EPISODE = 'tv_series_episode'
IS_NOT_MOVIE_OR_TV_SERIES = 'general'
TV_MULTIPLE = 'tv_multiple'
SEARCH_TYPE = 'search_type'
LANGUAGE = 'language'
DESCRIPTION = 'description'
EXTRA = 'extra'
DIRECTORY = 'directory'
EXTENSION = 'extension'
DOWNLOAD_FILE = 'file'
LINK = 'link'
DOWNLOAD_LINK = 'direct download'
FILE_TYPE = 'file type'
SITE = 'site'
SCORE = 'score'
FILENAME= 'filename'




def argument_parser():
    parser = optparse.OptionParser()

    ## Input File
    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="input",
                      default='Two.and.a.Half.Men.S09E21.HDTV.x264-LOL')

    ## Output File
    parser.add_option('-o', '--output',
                      action="store", dest="output",
                      help="output", default='output file etc ')

    ## Clipboard
    parser.add_option('-c', '--clipboard',
                      action="store", dest="clipboard",
                      help="clipboard", default='')

    ## Command
    parser.add_option('-r', '--run',
                      action="store", dest="command",
                      help="command", default='ls -l ~/Dropbox')

    ## Number
    parser.add_option("-n", type="int", dest="number")
    parser.add_option("-f", type="float", dest="float")

    ## Debug
    parser.add_option("-v", action="store_true", dest="verbose")

    options, args = parser.parse_args()

    return options