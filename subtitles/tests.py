"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import json

from django.test import TestCase, RequestFactory
from ratelimit.exceptions import Ratelimited
from subtitles.model.persistence import Persistence

class AddTestCase(TestCase):
    def testNoError(self):
        """Test that the ``add`` task runs with no errors,
        and returns the correct result."""


class DownloadTest(TestCase):
    def setUp(self):
        result_1 = {
            'rating': '0',
            'description': 'The.Walking.Dead.S02E06.HDTV.XviD-ASAP The.Walking',
            'format': 'srt',
            'sync': False,
            'site': 'Podnapisi',
            'link': '1370735',
            'language_flag': 'flags/.gif',
            'id': u'1370735',
            'movie': u'The Walking Dead',
            'extra': {'id': '1370735'},
            'language_id': '',
            'filename': u'The.Walking.Dead.S02E06.HDTV.XviD-ASAP The.Walking.Dead.S02E06.720p.HDTV.x264-IMMERSE',
            'hearing_imp': False,
            'score': 100.0,
            'release': 'The.Walking.Dead.S02E06.HDTV.XviD-ASAP The.Walking.Dead.S02E06.720p.HDTV.x264-IMMERSE',
            'language_name': '',
            'title': 'The.Walking.Dead',
            'season': '2',
            'episode': '6'
        }

        results = [
            result_1,
        ]
        results_iso_639_1 = 'es'
        results_hash_id = 'The Walking Dead;2;6;'
        results_site = 'Podnapisi'

        ##saving result
        Persistence.save_results(results, results_hash_id, results_iso_639_1, results_site)


        return

    def test_download_limits(self):
        from subtitles.views import download_by_id

        factory = RequestFactory()
        request = factory.get('download/id/')

        num_requests = 25
        for i in xrange(num_requests):
            response = download_by_id(request, 1)
            response_json = json.loads(response.content)
            self.assertTrue(response_json['status'], 'testing download view within the limits')

        ###################
        response = download_by_id(request, 1)
        response_json = json.loads(response.content)
        self.assertFalse(response_json['status'], 'testing download after reaching the limits')





        return