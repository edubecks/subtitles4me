from django.db import models
from datetime import datetime
from django.utils.timezone import utc

# Create your models here.
class Log(models.Model):
    id_caller = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=datetime.utcnow().replace(tzinfo=utc), blank=True)
    caller = models.CharField(max_length=100)
    message = models.CharField(max_length=400)


class Subtitle(models.Model):
    id_subtitle = models.AutoField(primary_key=True)

    hash_id = models.CharField(max_length=100)
    id = models.CharField(max_length=50)
    site = models.CharField(max_length=15)
    language_iso_639_1 = models.CharField(max_length=2)

    description = models.CharField(max_length=500, blank=True)
    download_link = models.CharField(max_length=150, null=True, blank=True)
    link = models.CharField(max_length=150, null=True, blank=True)

    extra = models.CharField(max_length=100, blank=True)

    def get_absolute_url(self):
        return 'download/id/' + str(self.id_subtitle)


    class Meta:
        unique_together = (('id','site', 'hash_id', 'language_iso_639_1'),)

class ResultsQueue(models.Model):
    hash_id = models.CharField(max_length=200, primary_key=True)
#    language_iso_639_1 = models.CharField(max_length=2)
    ## date added for future download updater
    date_added = models.DateTimeField(default=datetime.utcnow().replace(tzinfo=utc), blank=True)


class AnalyticsSearch(models.Model):
    filename = hash_id = models.CharField(max_length=150)
    hash_id = hash_id = models.CharField(max_length=100)
    counter = models.PositiveIntegerField(default=0)
    date_added = models.DateTimeField(default=datetime.utcnow().replace(tzinfo=utc), blank=True)
    has_results = models.BooleanField(default=False)

    def get_absolute_url(self):
        return '/search/'+self.filename.strip().replace(' ', '.')

    def modified(self):
        return self.date_added




class Statistics(models.Model):
    id_subtitle = models.ForeignKey(Subtitle)
    date_added = models.DateTimeField(default=datetime.utcnow().replace(tzinfo=utc), blank=True)
    download_count = models.PositiveIntegerField(default=0)
    score_positive = models.PositiveIntegerField(default=0)
    score_counter = models.PositiveIntegerField(default=0)
    score_negative = models.PositiveIntegerField(default=0)


class AnalyticsModel(models.Model):
    source = models.CharField(max_length=100, primary_key=True )
    counter = models.PositiveIntegerField(default=0)

