# coding: utf-8
__author__ = 'edubecks'

from django.contrib.sitemaps import Sitemap

class StaticSitemap(Sitemap):
    priority = 0.5
    lastmod = None

    def items(self):
        return [
                "/",
                "/contact/",
                "/about/",
                "/how-to-use",
                # ...
                # EX:
                # ("/opportunities", "daily"),
                # ("/people", "daily")
            ]

    def location(self, obj):
        return obj[0] if isinstance(obj, tuple) else obj

    def changefreq(self, obj):
        return obj[1] if isinstance(obj, tuple) else "monthly"

