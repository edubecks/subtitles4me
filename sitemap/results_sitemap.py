from datetime import datetime, timedelta
from django.contrib.sitemaps import Sitemap
from django.utils.timezone import utc
from subtitles.models import AnalyticsSearch

__author__ = 'edubecks'

class BaseSitemap(Sitemap):
    priority = 0.5
    changefreq = "weekly"

    def location(self, obj):
        return obj.get_absolute_url()

    def lastmod(self, obj):
        return obj.modified() or obj.created()


class ResultsSitemap(BaseSitemap):
    changefreq = "daily"

    def items(self):
        range_hours = 24 * 7 ## 1 week
        return AnalyticsSearch.objects.filter(
            date_added__gt=(datetime.utcnow().replace(tzinfo=utc) - timedelta(hours=range_hours)),
            has_results=True
            ).order_by('-date_added')