__author__ = 'edubecks'

from sitemap.results_sitemap import ResultsSitemap
from sitemap.static_sitemap import StaticSitemap

sitemaps = {
    'results': ResultsSitemap,
    'static': StaticSitemap,
}

