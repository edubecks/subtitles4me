#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Created on March 27, 2013
@author: edubecks
"""
import multiprocessing
import subprocess
import shlex

num_tors = 40
base_socks_port = 9050
base_control_port = 8110

def _run_tor(i):
    """
    to kill them
    kill -9 `ps -ef | grep /MacOS/tor | grep -v grep | awk '{print $2}'`
    """
    socks_port = base_socks_port + i
    control_port = base_control_port + i

    command = '/Applications/TorBrowser_en-US.app/Contents/MacOS/tor --RunAsDaemon 0 --CookieAuthentication 0 --HashedControlPassword "" --ControlPort ' + str(
        control_port) + ' --PidFile tor'+str(i)+'.pid --SocksPort ' + str(socks_port)+ ' --DataDirectory data/tor'+str(i)
    args = shlex.split(command)
    process = subprocess.Popen(args)
    return


def main():


    jobs = []
    for i in xrange(num_tors):
        job = multiprocessing.Process(
            target=_run_tor, args=(i, )
        )
        jobs.append(job)

    for job in jobs:
        job.start()



if __name__ == '__main__':
    main()