#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
Created on March 17, 2013
@author: edubecks
"""

import subprocess
import os
import sys

__author__ = 'edubecks'

def get_program_dir():
    """
    return the filesystem directory where the app program file
    resides
    (ie. the applications exec or import directory).
    """
    if hasattr(sys, 'frozen'):
        #app is frozen or bundled with py2app or py2exe or cxfreeze
        app_dir = os.path.dirname(sys.executable)
    else:
        if __name__ != '__main__': # we were imported
            app_dir = os.path.dirname(__file__)
        else: # we were exec'ed
            app_dir = os.path.abspath(sys.path[0])
    return app_dir

def execute_shell_script(command):

    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    if process.returncode == 0:
        print out
    else:
        print err


def crawler_links_script(selected_users, limit):
    APP_DIR = os.path.abspath(os.path.join(get_program_dir(),os.pardir))
    MANAGE_PY= os.path.abspath(os.path.join(APP_DIR,'manage.py'))
    for user in selected_users:
        command = 'python '+MANAGE_PY+' crawler_links --user '+str(user)+' --limit '+str(limit)
        print 'running '+ command
        execute_shell_script(command)
    return
