#!/bin/sh
## there must be a symbolic link to the project directory
## Ex:  ~/Dropbox/Developer/SubtitleDownloader/
# change to project directory
cd ~/SubtitleDownloader/;
# activate virtual env
source ~/virtual_env/python_django/bin/activate;
./manage.py schemamigration subtitles --auto
./manage.py migrate subtitles