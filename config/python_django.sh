#!/bin/bash
brew install git;
brew install readline sqlite gdbm;
brew install python --universal --framework;
easy_install pip 
pip install virtualenv 
mkdir ~/python_django;
cd ~/python_django;
virtualenv ~/python_django/python_django --no-site-packages;
source ~/python_django/python_django/bin/activate;
pip install django;