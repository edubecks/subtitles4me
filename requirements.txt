BeautifulSoup==3.2.1
Django==1.4.2
MySQL-python==1.2.3
Pygments==1.5
SocksiPy-branch==1.01
South==0.7.6
Unidecode==0.04.9
anyjson==0.3.3
billiard==2.7.3.19
boto==2.9.5
bpython==0.11
celery==3.0.13
coverage==3.6b1
crcmod==1.7
distribute==0.6.35
dj-database-url==0.2.1
django-braces==0.1.9
django-cache-machine==0.6
django-celery==3.0.11
django-common-helpers==0.3
django-contactme==1.0a2
django-coverage==1.2.2
django-crispy-forms==1.2.1
django-cron==0.2.2
django-debug-toolbar==0.9.4
django-extensions==1.0.1
django-floppyforms==1.0
django-heroku-memcacheify==0.3
django-heroku-postgresify==0.2
django-profiletools==0.2.1
django-pylibmc-sasl==0.2.4
django-ratelimit==0.3.0
django-registration==0.8
django-storages==1.1.5
feedparser==5.1.3
google-api-python-client==1.1
gsutil==3.29
gunicorn==0.16.1
heroku==0.1.2
httplib2==0.8
kombu==2.5.4
meld3==0.6.10
newrelic==1.10.0.28
profilehooks==1.6
psycopg2==2.4.6
pyOpenSSL==0.13
pylibmc==1.2.3
python-dateutil==1.5
python-gflags==2.0
pytz==2012j
raven==2.0.10
requests==1.1.0
stevedore==0.7.2
supervisor==3.0b1
virtualenv==1.8.4
virtualenv-clone==0.2.4
virtualenvwrapper==3.6
wsgiref==0.1.2
