# Django settings for SubtitleDownloader project.
import os
import socket
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
import dj_database_url
from subtitles.djangosettings import *



#import djcelery
#djcelery.setup_loader()




DEBUG = True
## hack anhadir computadores donde es desarrollado para q sea aceptado por heroku
if (
    socket.gethostname() != 'lcad69.lcad.icmc.usp.br'
    and socket.gethostname() != 'SpiderMac.local'
    and socket.gethostname() != 'myvhost.local'
    ## dennis cholopuxama
    and socket.gethostname() != 'puxama-H61M-USB3-B3'
    ):
    DEBUG = False
print 'DEBUG', DEBUG


TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Subtites4me', 'subtitles4me@gmail.com'),
)

MANAGERS = ADMINS




## AMAZON RDS: CAMBIAR AQUI
#DEBUG = False
AMAZON_RDS = False
# AMAZON_RDS = True
if AMAZON_RDS or not DEBUG:
#    print 'running in heroku'
    DATABASES = {
        'default': {
            'ENGINE':   PRODUCTION_DATABASE_ENGINE,
            'NAME':     PRODUCTION_DATABASE_NAME,                      # Or path to database file if using sqlite3.
            'USER':     PRODUCTION_DATABASE_USER,                      # Not used with sqlite3.
            'PASSWORD': PRODUCTION_DATABASE_PASSWORD,                  # Not used with sqlite3.
            'HOST':     PRODUCTION_DATABASE_HOST,                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT':     PRODUCTION_DATABASE_PORT,                      # Set to empty string for default. Not used with sqlite3.
        },
    }
    ## HEROKU
#    DATABASES = {                      *
#        'default' : dj_database_url.config()
#    }
else:
    DATABASES = {
        'default': {
            'ENGINE': DEV_DATABASE_ENGINE,
            'NAME': DEV_DATABASE_NAME,                      # Or path to database file if using sqlite3.
            'USER': DEV_DATABASE_USER,                      # Not used with sqlite3.
            'PASSWORD': DEV_DATABASE_PASSWORD,                  # Not used with sqlite3.
            'HOST': DEV_DATABASE_HOST,                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': DEV_DATABASE_PORT,                      # Set to empty string for default. Not used with sqlite3.
        },
    }


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Sao_Paulo'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars accrding to the current locale.
USE_L10N = False

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'  ## alias
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
PROJECT_DIR= os.path.abspath(os.path.join(PROJECT_ROOT, os.path.pardir))
#PROJECT_DIR = os.path.join(PROJECT_ROOT,'../blogapp')
STATIC_ROOT = os.path.abspath(os.path.join(PROJECT_DIR,'staticfiles/'))
#print 'STATIC_URL', STATIC_URL
#print 'STATIC_ROOT', STATIC_ROOT
#print 'PROJECT_ROOT', PROJECT_ROOT
#print 'PROJECT_DIR', PROJECT_DIR

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.abspath(os.path.join(PROJECT_DIR,'static/min/')),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)
#print STATICFILES_DIRS

TEMPLATE_CONTEXT_PROCESSORS += (
#TEMPLATE_CONTEXT_PROCESSORS = (
#    'django.core.context_processors.debug',
#    'django.core.context_processors.i18n',
#    'django.core.context_processors.media',
#    'django.core.context_processors.static',
#    'django.contrib.auth.context_processors.auth',
#    'django.contrib.messages.context_processors.messages',
    'SubtitleDownloader.context_processors.properties.request_variables',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '-r6h=^)9sj9i*h3-ae1e%7fz-au7(!+@ofg90cc*8qih758x(8'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
#    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'SubtitleDownloader.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'SubtitleDownloader.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
    )


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.sitemaps',
    'subtitles',
    'south',
    'gunicorn',
    #    'django_contactme',
    #    'kombu.transport.django',
    #    'djcelery',

)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

## disable south migrate
SOUTH_TESTS_MIGRATE = False

#CELERY_ALWAYS_EAGER = True
#TEST_RUNNER = 'djcelery.contrib.test_runner.CeleryTestSuiteRunner'
