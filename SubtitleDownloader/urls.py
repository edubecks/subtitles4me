from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic.simple import direct_to_template
from subtitles.views import *
from sitemap.sitemaps import sitemaps
import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', index),
    url(r'^opensearch\.xml$', direct_to_template,
        {'template': 'opensearch.xml',
#         'mimetype': 'application/opensearchdescription+xml'
         'mimetype': 'text/xml'
        },
        name="opensearch"),

    ##sitemap
    url(  # --- add this tuple
       r'^sitemap\.xml$',
       'django.contrib.sitemaps.views.sitemap',
       {'sitemaps': sitemaps}
        ),


    ## robots txt
    (r'^robots\.txt$', direct_to_template,
     {'template': 'robots.txt', 'mimetype': 'text/plain'}),

# url(r'^SubtitleDownloader/', include('SubtitleDownloader.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
#    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
#    url(r'^admin/', include(admin.site.urls)),

    ## download
#    url(r'^download/$',download_subtitles, name='download'),
#    url(r'^download/filename/(.*)/(json)', search_and_download),
#    url(r'^download/site/(.*)/id/(.*)/$', download),
    url(r'^download/id/(.*)$', download_by_id),
#    url(r'^download/(.*)',download_subtitles),


    ## search
    url(r'^search/$', search),

    url(r'^search/(.*)$', search_filename),

    ##menu
    url(r'^about/$', about),
    url(r'^how-to-use/$', how_to_use),
    # url(r'^last-subtitles/$', last_results),

    ## contact
    url(r'^contact/', contact),
#    url(r'^contact/', include('django_contactme.urls')),

    url(
        regex=r'^static/(?P<path>.*)$',
        view='django.views.static.serve',
        kwargs={'document_root': settings.STATIC_ROOT,}
    ),

    ##testing/debugging
#    url(r'^test/$', results),
#    url(r'^test/worker/$', test_worker),
)

